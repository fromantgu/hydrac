from runpy import run_path
import sys
from pathlib import Path
import os


from setuptools import setup, find_packages
from setuptools.dist import Distribution

if sys.version_info[:2] < (3, 6):
    raise RuntimeError("Python version >= 3.6 required.")


def install_setup_requires():
    dist = Distribution()
    # Honor setup.cfg's options.
    dist.parse_config_files(ignore_option_errors=True)
    if dist.setup_requires:
        dist.fetch_build_eggs(dist.setup_requires)


install_setup_requires()

here = Path(__file__).parent.absolute()

# Get the long description from the relevant file
with open(here / "README.rst") as file:
    long_description = file.read()
lines = long_description.splitlines(True)
long_description = "".join(lines[14:])

# Get the version from the relevant file
d = run_path("hydrac/_version.py")
__version__ = d["__version__"]

# Get the development status from the version string
if "a" in __version__:
    devstatus = "Development Status :: 3 - Alpha"
elif "b" in __version__:
    devstatus = "Development Status :: 4 - Beta"
else:
    devstatus = "Development Status :: 5 - Production/Stable"


install_requires = ["numpy", "scipy", "transonic", "h5py", "matplotlib"]
# Even though we also use scipy, we don't require its installation
# because it can be heavy to install.

from setup_build import HydracBuildExt

def transonize():

    from transonic.dist import make_backend_files

    paths = [
        "hydrac/model/scattering/dwba.py",
    ]
    make_backend_files([here / path for path in paths])


def create_pythran_extensions():
    import numpy as np
    from transonic.dist import init_pythran_extensions

    compile_arch = os.getenv("CARCH", "native")
    extensions = init_pythran_extensions(
        "hydrac",
        include_dirs=np.get_include(),
        compile_args=("-O3", f"-march={compile_arch}", "-DUSE_XSIMD"),
    )
    return extensions


def create_extensions():
    if "egg_info" in sys.argv:
        return []

    transonize()

    ext_modules = create_pythran_extensions()

    return ext_modules

setup(
    name="hydrac",
    version=__version__,
    description=(
        "framework for inverting acoustic signal into mass concentration."
    ),
    long_description=long_description,
    keywords="Acoustics, research",
    author="Guillaume Fromant",
    author_email="guillaume.fromant@legi.cnrs.fr",
    url="https://bitbucket.org/hydrac/hydrac/",
    license="CeCILL",
    classifiers=[
        # How mature is this project? Common values are
        # 3 - Alpha
        # 4 - Beta
        # 5 - Production/Stable
        devstatus,
        "Intended Audience :: Science/Research",
        "Intended Audience :: Education",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: BSD License",
        # actually CeCILL-B License (BSD compatible license for French laws)
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    packages=find_packages(exclude=["doc"]),
    package_data={
        "hydrac": [
            "hydrac/util/models_presets/*.xml",
            "hydrac/util/adcp/*.xml",
            "hydrac/util/aqa/*.xml",
            "hydrac/util/ub_mes/*.xml",
        ]
    },
    include_package_data=True,
    install_requires=install_requires,
    cmdclass={"build_ext": HydracBuildExt},
    ext_modules=create_extensions(),
)
