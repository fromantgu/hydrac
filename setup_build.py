from pathlib import Path
from runpy import run_path

from transonic.dist import ParallelBuildExt

here = Path(__file__).parent.absolute()

class HydracBuildExt(ParallelBuildExt):
    def initialize_options(self):
        super().initialize_options()
        self.logger_name = "fluidsim"
        self.num_jobs_env_var = "FLUIDDYN_NUM_PROCS_BUILD"

    def get_num_jobs(self):
        return None
