======
Hydrac
======

|release| |docs| |coverage|

.. |release| image:: https://img.shields.io/pypi/v/fluiddyn.svg
   :target: https://test.pypi.org/project/hydrac/
   :alt: Latest version

.. |docs| image:: https://readthedocs.org/projects/hydrac/badge/?version=latest
   :target: http://hydrac.readthedocs.org
   :alt: Documentation status


Hydrac is a framework for interpreting acoustic signals from multiple acoustic instruments into suspended sediment concentration and size distribution. The Python
package hydrac contains basic utilities (file io, figures,
etc.).  

*Key words and ambitions*: fluid dynamics research with Python (>= 3.4);
modular, object-oriented, collaborative, tested and documented, free and
open-source software.

License
-------

Hydrac is distributed under the CeCILL-B_ License, a BSD compatible
french license.

.. _CeCILL-B: http://www.cecill.info/index.en.html

Installation
------------

It is recommended to `install numpy <http://scipy.org/install.html>`_ before
installing hydrac. The simplest way to install hydrac is by using pip::

  pip install hydrac==0.0.2

You can get the source code from `Bitbucket
<https://fromantgu@bitbucket.org/fromantgu/hydrac>`_


History
-------


