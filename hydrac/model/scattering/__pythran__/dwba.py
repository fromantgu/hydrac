import numpy as np
from scipy import special

# pythran export DWBAintegrand(float, float64[:], float64[:,:], float64[:,:], float64[:], float64[:], float64[:], float, float, float, float)


def DWBAintegrand(s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2):
    #def DWBAintegrand(self, s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2):
    '\n    Integration function. Needs to be simplifyed in the future to make it\n    low level function.\n    '
    i = np.complex(0, 1)
    rposx = ((s * (rpos2[(0, 0)] - rpos1[(0, 0)])) + rpos1[(0, 0)])
    rposy = ((s * (rpos2[(1, 0)] - rpos1[(1, 0)])) + rpos1[(1, 0)])
    rposz = ((s * (rpos2[(2, 0)] - rpos1[(2, 0)])) + rpos1[(2, 0)])
    rpos = np.array([rposx, rposy, rposz]).T
    a = ((s * (a2 - a1)) + a1)
    g = ((s * (g2 - g1)) + g1)
    h = ((s * (h2 - h1)) + h1)
    gamgam = (((1 / ((g * h) * h)) + (1 / g)) - 2)
    if (np.abs((np.abs(betatilt) - (np.pi / 2))) < 1e-10):
        # limiting case for end-on incidence
        bessy = ((np.linalg.norm(k1) * a) / h)
    else:
        bessy = (special.jv(1, ((((2 * np.linalg.norm(k1)) * a) / h) * np.cos(betatilt))) / np.cos(betatilt))
    return ((((((np.linalg.norm(k1) / 4) * gamgam) * np.exp((((2 * i) * np.dot(k1.T, rpos)) / h))) * a) * bessy) * np.linalg.norm((rpos2 - rpos1)))

# pythran export __transonic__
__transonic__ = ('0.2.4',)