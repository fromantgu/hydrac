#include <pythonic/core.hpp>
#include <pythonic/python/core.hpp>
#include <pythonic/types/bool.hpp>
#include <pythonic/types/int.hpp>
#ifdef _OPENMP
#include <omp.h>
#endif
#include <pythonic/include/types/numpy_texpr.hpp>
#include <pythonic/include/types/float64.hpp>
#include <pythonic/include/types/ndarray.hpp>
#include <pythonic/include/types/float.hpp>
#include <pythonic/types/float.hpp>
#include <pythonic/types/numpy_texpr.hpp>
#include <pythonic/types/ndarray.hpp>
#include <pythonic/types/float64.hpp>
#include <pythonic/include/__builtin__/getattr.hpp>
#include <pythonic/include/__builtin__/pythran/static_list.hpp>
#include <pythonic/include/__builtin__/tuple.hpp>
#include <pythonic/include/numpy/abs.hpp>
#include <pythonic/include/numpy/array.hpp>
#include <pythonic/include/numpy/cos.hpp>
#include <pythonic/include/numpy/dot.hpp>
#include <pythonic/include/numpy/exp.hpp>
#include <pythonic/include/numpy/linalg/norm.hpp>
#include <pythonic/include/operator_/add.hpp>
#include <pythonic/include/operator_/div.hpp>
#include <pythonic/include/operator_/idiv.hpp>
#include <pythonic/include/operator_/lt.hpp>
#include <pythonic/include/operator_/mul.hpp>
#include <pythonic/include/scipy/special/jv.hpp>
#include <pythonic/include/types/complex.hpp>
#include <pythonic/include/types/str.hpp>
#include <pythonic/__builtin__/getattr.hpp>
#include <pythonic/__builtin__/pythran/static_list.hpp>
#include <pythonic/__builtin__/tuple.hpp>
#include <pythonic/numpy/abs.hpp>
#include <pythonic/numpy/array.hpp>
#include <pythonic/numpy/cos.hpp>
#include <pythonic/numpy/dot.hpp>
#include <pythonic/numpy/exp.hpp>
#include <pythonic/numpy/linalg/norm.hpp>
#include <pythonic/operator_/add.hpp>
#include <pythonic/operator_/div.hpp>
#include <pythonic/operator_/idiv.hpp>
#include <pythonic/operator_/lt.hpp>
#include <pythonic/operator_/mul.hpp>
#include <pythonic/scipy/special/jv.hpp>
#include <pythonic/types/complex.hpp>
#include <pythonic/types/str.hpp>
namespace __pythran_dwba
{
  struct __transonic__
  {
    typedef void callable;
    typedef void pure;
    struct type
    {
      typedef pythonic::types::str __type0;
      typedef typename pythonic::returnable<decltype(pythonic::types::make_tuple(std::declval<__type0>()))>::type result_type;
    }  ;
    typename type::result_type operator()() const;
    ;
  }  ;
  struct DWBAintegrand
  {
    typedef void callable;
    ;
    template <typename argument_type0 , typename argument_type1 , typename argument_type2 , typename argument_type3 , typename argument_type4 , typename argument_type5 , typename argument_type6 , typename argument_type7 , typename argument_type8 , typename argument_type9 , typename argument_type10 >
    struct type
    {
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::linalg::functor::norm{})>::type>::type __type0;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type1>::type>::type __type1;
      typedef decltype(std::declval<__type0>()(std::declval<__type1>())) __type2;
      typedef long __type3;
      typedef decltype((pythonic::operator_::div(std::declval<__type2>(), std::declval<__type3>()))) __type4;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type0>::type>::type __type5;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type9>::type>::type __type6;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type7>::type>::type __type7;
      typedef decltype((std::declval<__type6>() - std::declval<__type7>())) __type8;
      typedef decltype((pythonic::operator_::mul(std::declval<__type5>(), std::declval<__type8>()))) __type9;
      typedef typename pythonic::assignable<decltype((pythonic::operator_::add(std::declval<__type9>(), std::declval<__type7>())))>::type __type10;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type10>::type>::type __type11;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type8>::type>::type __type12;
      typedef decltype((std::declval<__type11>() - std::declval<__type12>())) __type13;
      typedef decltype((pythonic::operator_::mul(std::declval<__type5>(), std::declval<__type13>()))) __type14;
      typedef typename pythonic::assignable<decltype((pythonic::operator_::add(std::declval<__type14>(), std::declval<__type12>())))>::type __type15;
      typedef decltype((pythonic::operator_::mul(std::declval<__type10>(), std::declval<__type15>()))) __type16;
      typedef decltype((pythonic::operator_::mul(std::declval<__type16>(), std::declval<__type15>()))) __type17;
      typedef decltype((pythonic::operator_::div(std::declval<__type3>(), std::declval<__type17>()))) __type18;
      typedef decltype((pythonic::operator_::div(std::declval<__type3>(), std::declval<__type10>()))) __type19;
      typedef decltype((pythonic::operator_::add(std::declval<__type18>(), std::declval<__type19>()))) __type20;
      typedef decltype((std::declval<__type20>() - std::declval<__type3>())) __type21;
      typedef decltype((pythonic::operator_::mul(std::declval<__type4>(), std::declval<__type21>()))) __type22;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::functor::exp{})>::type>::type __type23;
      typedef std::complex<double> __type24;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::functor::dot{})>::type>::type __type25;
      typedef decltype(pythonic::__builtin__::getattr(pythonic::types::attr::T{}, std::declval<__type1>())) __type26;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::functor::array{})>::type>::type __type27;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::__builtin__::pythran::functor::static_list{})>::type>::type __type28;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type3>::type>::type __type29;
      typedef decltype(pythonic::types::make_tuple(std::declval<__type3>(), std::declval<__type3>())) __type30;
      typedef decltype(std::declval<__type29>()[std::declval<__type30>()]) __type31;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type2>::type>::type __type32;
      typedef decltype(std::declval<__type32>()[std::declval<__type30>()]) __type33;
      typedef decltype((std::declval<__type31>() - std::declval<__type33>())) __type34;
      typedef decltype((pythonic::operator_::mul(std::declval<__type5>(), std::declval<__type34>()))) __type35;
      typedef decltype((pythonic::operator_::add(std::declval<__type35>(), std::declval<__type33>()))) __type36;
      typedef decltype(pythonic::types::make_tuple(std::declval<__type36>(), std::declval<__type36>(), std::declval<__type36>())) __type37;
      typedef decltype(std::declval<__type28>()(std::declval<__type37>())) __type38;
      typedef decltype(std::declval<__type27>()(std::declval<__type38>())) __type39;
      typedef decltype(pythonic::__builtin__::getattr(pythonic::types::attr::T{}, std::declval<__type39>())) __type40;
      typedef decltype(std::declval<__type25>()(std::declval<__type26>(), std::declval<__type40>())) __type41;
      typedef decltype((pythonic::operator_::mul(std::declval<__type24>(), std::declval<__type41>()))) __type42;
      typedef decltype((pythonic::operator_::div(std::declval<__type42>(), std::declval<__type15>()))) __type43;
      typedef decltype(std::declval<__type23>()(std::declval<__type43>())) __type44;
      typedef decltype((pythonic::operator_::mul(std::declval<__type22>(), std::declval<__type44>()))) __type45;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type5>::type>::type __type46;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type4>::type>::type __type47;
      typedef decltype((std::declval<__type46>() - std::declval<__type47>())) __type48;
      typedef decltype((pythonic::operator_::mul(std::declval<__type5>(), std::declval<__type48>()))) __type49;
      typedef typename pythonic::assignable<decltype((pythonic::operator_::add(std::declval<__type49>(), std::declval<__type47>())))>::type __type50;
      typedef decltype((pythonic::operator_::mul(std::declval<__type45>(), std::declval<__type50>()))) __type51;
      typedef decltype((pythonic::operator_::mul(std::declval<__type2>(), std::declval<__type50>()))) __type52;
      typedef typename pythonic::assignable<decltype((pythonic::operator_::div(std::declval<__type52>(), std::declval<__type15>())))>::type __type53;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::scipy::special::functor::jv{})>::type>::type __type54;
      typedef decltype((pythonic::operator_::mul(std::declval<__type3>(), std::declval<__type2>()))) __type55;
      typedef decltype((pythonic::operator_::mul(std::declval<__type55>(), std::declval<__type50>()))) __type56;
      typedef decltype((pythonic::operator_::div(std::declval<__type56>(), std::declval<__type15>()))) __type57;
      typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::functor::cos{})>::type>::type __type58;
      typedef typename std::remove_cv<typename std::remove_reference<argument_type6>::type>::type __type59;
      typedef decltype(std::declval<__type58>()(std::declval<__type59>())) __type60;
      typedef decltype((pythonic::operator_::mul(std::declval<__type57>(), std::declval<__type60>()))) __type61;
      typedef decltype(std::declval<__type54>()(std::declval<__type3>(), std::declval<__type61>())) __type62;
      typedef typename pythonic::assignable<decltype((pythonic::operator_::div(std::declval<__type62>(), std::declval<__type60>())))>::type __type63;
      typedef typename __combined<__type53,__type63>::type __type64;
      typedef decltype((pythonic::operator_::mul(std::declval<__type51>(), std::declval<__type64>()))) __type65;
      typedef decltype((std::declval<__type29>() - std::declval<__type32>())) __type66;
      typedef decltype(std::declval<__type0>()(std::declval<__type66>())) __type67;
      typedef typename pythonic::returnable<decltype((pythonic::operator_::mul(std::declval<__type65>(), std::declval<__type67>())))>::type result_type;
    }  
    ;
    template <typename argument_type0 , typename argument_type1 , typename argument_type2 , typename argument_type3 , typename argument_type4 , typename argument_type5 , typename argument_type6 , typename argument_type7 , typename argument_type8 , typename argument_type9 , typename argument_type10 >
    typename type<argument_type0, argument_type1, argument_type2, argument_type3, argument_type4, argument_type5, argument_type6, argument_type7, argument_type8, argument_type9, argument_type10>::result_type operator()(argument_type0&& s, argument_type1&& k1, argument_type2&& rpos1, argument_type3&& rpos2, argument_type4&& a1, argument_type5&& a2, argument_type6&& betatilt, argument_type7&& g1, argument_type8&& h1, argument_type9&& g2, argument_type10&& h2) const
    ;
  }  ;
  typename __transonic__::type::result_type __transonic__::operator()() const
  {
    {
      static typename __transonic__::type::result_type tmp_global = pythonic::types::make_tuple(pythonic::types::str("0.2.4"));
      return tmp_global;
    }
  }
  template <typename argument_type0 , typename argument_type1 , typename argument_type2 , typename argument_type3 , typename argument_type4 , typename argument_type5 , typename argument_type6 , typename argument_type7 , typename argument_type8 , typename argument_type9 , typename argument_type10 >
  typename DWBAintegrand::type<argument_type0, argument_type1, argument_type2, argument_type3, argument_type4, argument_type5, argument_type6, argument_type7, argument_type8, argument_type9, argument_type10>::result_type DWBAintegrand::operator()(argument_type0&& s, argument_type1&& k1, argument_type2&& rpos1, argument_type3&& rpos2, argument_type4&& a1, argument_type5&& a2, argument_type6&& betatilt, argument_type7&& g1, argument_type8&& h1, argument_type9&& g2, argument_type10&& h2) const
  {
    typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::linalg::functor::norm{})>::type>::type __type0;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type1>::type>::type __type1;
    typedef decltype(std::declval<__type0>()(std::declval<__type1>())) __type2;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type0>::type>::type __type3;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type5>::type>::type __type4;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type4>::type>::type __type5;
    typedef decltype((std::declval<__type4>() - std::declval<__type5>())) __type6;
    typedef decltype((pythonic::operator_::mul(std::declval<__type3>(), std::declval<__type6>()))) __type7;
    typedef typename pythonic::assignable<decltype((pythonic::operator_::add(std::declval<__type7>(), std::declval<__type5>())))>::type __type8;
    typedef decltype((pythonic::operator_::mul(std::declval<__type2>(), std::declval<__type8>()))) __type9;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type10>::type>::type __type10;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type8>::type>::type __type11;
    typedef decltype((std::declval<__type10>() - std::declval<__type11>())) __type12;
    typedef decltype((pythonic::operator_::mul(std::declval<__type3>(), std::declval<__type12>()))) __type13;
    typedef typename pythonic::assignable<decltype((pythonic::operator_::add(std::declval<__type13>(), std::declval<__type11>())))>::type __type14;
    typedef typename pythonic::assignable<decltype((pythonic::operator_::div(std::declval<__type9>(), std::declval<__type14>())))>::type __type15;
    typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::scipy::special::functor::jv{})>::type>::type __type16;
    typedef long __type17;
    typedef decltype((pythonic::operator_::mul(std::declval<__type17>(), std::declval<__type2>()))) __type18;
    typedef decltype((pythonic::operator_::mul(std::declval<__type18>(), std::declval<__type8>()))) __type19;
    typedef decltype((pythonic::operator_::div(std::declval<__type19>(), std::declval<__type14>()))) __type20;
    typedef typename std::remove_cv<typename std::remove_reference<decltype(pythonic::numpy::functor::cos{})>::type>::type __type21;
    typedef typename std::remove_cv<typename std::remove_reference<argument_type6>::type>::type __type22;
    typedef decltype(std::declval<__type21>()(std::declval<__type22>())) __type23;
    typedef decltype((pythonic::operator_::mul(std::declval<__type20>(), std::declval<__type23>()))) __type24;
    typedef decltype(std::declval<__type16>()(std::declval<__type17>(), std::declval<__type24>())) __type25;
    typedef typename pythonic::assignable<decltype((pythonic::operator_::div(std::declval<__type25>(), std::declval<__type23>())))>::type __type26;
    typename pythonic::assignable<typename __combined<__type15,__type26>::type>::type bessy;
    ;
    ;
    ;
    ;
    ;
    typename pythonic::assignable<decltype((pythonic::operator_::add((pythonic::operator_::mul(s, (a2 - a1))), a1)))>::type a = (pythonic::operator_::add((pythonic::operator_::mul(s, (a2 - a1))), a1));
    typename pythonic::assignable<decltype((pythonic::operator_::add((pythonic::operator_::mul(s, (g2 - g1))), g1)))>::type g = (pythonic::operator_::add((pythonic::operator_::mul(s, (g2 - g1))), g1));
    typename pythonic::assignable<decltype((pythonic::operator_::add((pythonic::operator_::mul(s, (h2 - h1))), h1)))>::type h = (pythonic::operator_::add((pythonic::operator_::mul(s, (h2 - h1))), h1));
    ;
    if ((pythonic::operator_::lt(pythonic::numpy::functor::abs{}((pythonic::numpy::functor::abs{}(betatilt) - 1.5707963267948966)), 1e-10)))
    {
      bessy = (pythonic::operator_::div((pythonic::operator_::mul(pythonic::numpy::linalg::functor::norm{}(k1), a)), h));
    }
    else
    {
      bessy = (pythonic::operator_::div(pythonic::scipy::special::functor::jv{}(1L, (pythonic::operator_::mul((pythonic::operator_::div((pythonic::operator_::mul((pythonic::operator_::mul(2L, pythonic::numpy::linalg::functor::norm{}(k1))), a)), h)), pythonic::numpy::functor::cos{}(betatilt)))), pythonic::numpy::functor::cos{}(betatilt)));
    }
    return (pythonic::operator_::mul((pythonic::operator_::mul((pythonic::operator_::mul((pythonic::operator_::mul((pythonic::operator_::mul((pythonic::operator_::div(pythonic::numpy::linalg::functor::norm{}(k1), 4L)), ((pythonic::operator_::add((pythonic::operator_::div(1L, (pythonic::operator_::mul((pythonic::operator_::mul(g, h)), h)))), (pythonic::operator_::div(1L, g)))) - 2L))), pythonic::numpy::functor::exp{}((pythonic::operator_::div((pythonic::operator_::mul(std::complex<double>(0.0, 2.0), pythonic::numpy::functor::dot{}(pythonic::__builtin__::getattr(pythonic::types::attr::T{}, k1), pythonic::__builtin__::getattr(pythonic::types::attr::T{}, pythonic::numpy::functor::array{}(pythonic::__builtin__::pythran::functor::static_list{}(pythonic::types::make_tuple((pythonic::operator_::add((pythonic::operator_::mul(s, (rpos2[pythonic::types::make_tuple(0L, 0L)] - rpos1[pythonic::types::make_tuple(0L, 0L)]))), rpos1[pythonic::types::make_tuple(0L, 0L)])), (pythonic::operator_::add((pythonic::operator_::mul(s, (rpos2[pythonic::types::make_tuple(1L, 0L)] - rpos1[pythonic::types::make_tuple(1L, 0L)]))), rpos1[pythonic::types::make_tuple(1L, 0L)])), (pythonic::operator_::add((pythonic::operator_::mul(s, (rpos2[pythonic::types::make_tuple(2L, 0L)] - rpos1[pythonic::types::make_tuple(2L, 0L)]))), rpos1[pythonic::types::make_tuple(2L, 0L)]))))))))), h))))), a)), bessy)), pythonic::numpy::linalg::functor::norm{}((rpos2 - rpos1))));
  }
}
#include <pythonic/python/exception_handler.hpp>
#ifdef ENABLE_PYTHON_MODULE
static PyObject* __transonic__ = to_python(__pythran_dwba::__transonic__()());
typename __pythran_dwba::DWBAintegrand::type<double, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, double, double, double, double>::result_type DWBAintegrand0(double&& s, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& k1, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>&& rpos1, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>&& rpos2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a1, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& betatilt, double&& g1, double&& h1, double&& g2, double&& h2) 
{
  
                            PyThreadState *_save = PyEval_SaveThread();
                            try {
                                auto res = __pythran_dwba::DWBAintegrand()(s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2);
                                PyEval_RestoreThread(_save);
                                return res;
                            }
                            catch(...) {
                                PyEval_RestoreThread(_save);
                                throw;
                            }
                            ;
}
typename __pythran_dwba::DWBAintegrand::type<double, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, double, double, double, double>::result_type DWBAintegrand1(double&& s, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& k1, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>&& rpos1, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>&& rpos2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a1, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& betatilt, double&& g1, double&& h1, double&& g2, double&& h2) 
{
  
                            PyThreadState *_save = PyEval_SaveThread();
                            try {
                                auto res = __pythran_dwba::DWBAintegrand()(s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2);
                                PyEval_RestoreThread(_save);
                                return res;
                            }
                            catch(...) {
                                PyEval_RestoreThread(_save);
                                throw;
                            }
                            ;
}
typename __pythran_dwba::DWBAintegrand::type<double, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, double, double, double, double>::result_type DWBAintegrand2(double&& s, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& k1, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>&& rpos1, pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>&& rpos2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a1, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& betatilt, double&& g1, double&& h1, double&& g2, double&& h2) 
{
  
                            PyThreadState *_save = PyEval_SaveThread();
                            try {
                                auto res = __pythran_dwba::DWBAintegrand()(s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2);
                                PyEval_RestoreThread(_save);
                                return res;
                            }
                            catch(...) {
                                PyEval_RestoreThread(_save);
                                throw;
                            }
                            ;
}
typename __pythran_dwba::DWBAintegrand::type<double, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, pythonic::types::ndarray<double,pythonic::types::pshape<long>>, double, double, double, double>::result_type DWBAintegrand3(double&& s, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& k1, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>&& rpos1, pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>&& rpos2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a1, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& a2, pythonic::types::ndarray<double,pythonic::types::pshape<long>>&& betatilt, double&& g1, double&& h1, double&& g2, double&& h2) 
{
  
                            PyThreadState *_save = PyEval_SaveThread();
                            try {
                                auto res = __pythran_dwba::DWBAintegrand()(s, k1, rpos1, rpos2, a1, a2, betatilt, g1, h1, g2, h2);
                                PyEval_RestoreThread(_save);
                                return res;
                            }
                            catch(...) {
                                PyEval_RestoreThread(_save);
                                throw;
                            }
                            ;
}

static PyObject *
__pythran_wrap_DWBAintegrand0(PyObject *self, PyObject *args, PyObject *kw)
{
    PyObject* args_obj[11+1];
    char const* keywords[] = {"s","k1","rpos1","rpos2","a1","a2","betatilt","g1","h1","g2","h2", nullptr};
    if(! PyArg_ParseTupleAndKeywords(args, kw, "OOOOOOOOOOO",
                                     (char**)keywords, &args_obj[0], &args_obj[1], &args_obj[2], &args_obj[3], &args_obj[4], &args_obj[5], &args_obj[6], &args_obj[7], &args_obj[8], &args_obj[9], &args_obj[10]))
        return nullptr;
    if(is_convertible<double>(args_obj[0]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[2]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[3]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]) && is_convertible<double>(args_obj[7]) && is_convertible<double>(args_obj[8]) && is_convertible<double>(args_obj[9]) && is_convertible<double>(args_obj[10]))
        return to_python(DWBAintegrand0(from_python<double>(args_obj[0]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[2]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[3]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]), from_python<double>(args_obj[7]), from_python<double>(args_obj[8]), from_python<double>(args_obj[9]), from_python<double>(args_obj[10])));
    else {
        return nullptr;
    }
}

static PyObject *
__pythran_wrap_DWBAintegrand1(PyObject *self, PyObject *args, PyObject *kw)
{
    PyObject* args_obj[11+1];
    char const* keywords[] = {"s","k1","rpos1","rpos2","a1","a2","betatilt","g1","h1","g2","h2", nullptr};
    if(! PyArg_ParseTupleAndKeywords(args, kw, "OOOOOOOOOOO",
                                     (char**)keywords, &args_obj[0], &args_obj[1], &args_obj[2], &args_obj[3], &args_obj[4], &args_obj[5], &args_obj[6], &args_obj[7], &args_obj[8], &args_obj[9], &args_obj[10]))
        return nullptr;
    if(is_convertible<double>(args_obj[0]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[2]) && is_convertible<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[3]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]) && is_convertible<double>(args_obj[7]) && is_convertible<double>(args_obj[8]) && is_convertible<double>(args_obj[9]) && is_convertible<double>(args_obj[10]))
        return to_python(DWBAintegrand1(from_python<double>(args_obj[0]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[2]), from_python<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[3]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]), from_python<double>(args_obj[7]), from_python<double>(args_obj[8]), from_python<double>(args_obj[9]), from_python<double>(args_obj[10])));
    else {
        return nullptr;
    }
}

static PyObject *
__pythran_wrap_DWBAintegrand2(PyObject *self, PyObject *args, PyObject *kw)
{
    PyObject* args_obj[11+1];
    char const* keywords[] = {"s","k1","rpos1","rpos2","a1","a2","betatilt","g1","h1","g2","h2", nullptr};
    if(! PyArg_ParseTupleAndKeywords(args, kw, "OOOOOOOOOOO",
                                     (char**)keywords, &args_obj[0], &args_obj[1], &args_obj[2], &args_obj[3], &args_obj[4], &args_obj[5], &args_obj[6], &args_obj[7], &args_obj[8], &args_obj[9], &args_obj[10]))
        return nullptr;
    if(is_convertible<double>(args_obj[0]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]) && is_convertible<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[2]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[3]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]) && is_convertible<double>(args_obj[7]) && is_convertible<double>(args_obj[8]) && is_convertible<double>(args_obj[9]) && is_convertible<double>(args_obj[10]))
        return to_python(DWBAintegrand2(from_python<double>(args_obj[0]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]), from_python<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[2]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>(args_obj[3]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]), from_python<double>(args_obj[7]), from_python<double>(args_obj[8]), from_python<double>(args_obj[9]), from_python<double>(args_obj[10])));
    else {
        return nullptr;
    }
}

static PyObject *
__pythran_wrap_DWBAintegrand3(PyObject *self, PyObject *args, PyObject *kw)
{
    PyObject* args_obj[11+1];
    char const* keywords[] = {"s","k1","rpos1","rpos2","a1","a2","betatilt","g1","h1","g2","h2", nullptr};
    if(! PyArg_ParseTupleAndKeywords(args, kw, "OOOOOOOOOOO",
                                     (char**)keywords, &args_obj[0], &args_obj[1], &args_obj[2], &args_obj[3], &args_obj[4], &args_obj[5], &args_obj[6], &args_obj[7], &args_obj[8], &args_obj[9], &args_obj[10]))
        return nullptr;
    if(is_convertible<double>(args_obj[0]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]) && is_convertible<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[2]) && is_convertible<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[3]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]) && is_convertible<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]) && is_convertible<double>(args_obj[7]) && is_convertible<double>(args_obj[8]) && is_convertible<double>(args_obj[9]) && is_convertible<double>(args_obj[10]))
        return to_python(DWBAintegrand3(from_python<double>(args_obj[0]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[1]), from_python<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[2]), from_python<pythonic::types::numpy_texpr<pythonic::types::ndarray<double,pythonic::types::pshape<long,long>>>>(args_obj[3]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[4]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[5]), from_python<pythonic::types::ndarray<double,pythonic::types::pshape<long>>>(args_obj[6]), from_python<double>(args_obj[7]), from_python<double>(args_obj[8]), from_python<double>(args_obj[9]), from_python<double>(args_obj[10])));
    else {
        return nullptr;
    }
}

            static PyObject *
            __pythran_wrapall_DWBAintegrand(PyObject *self, PyObject *args, PyObject *kw)
            {
                return pythonic::handle_python_exception([self, args, kw]()
                -> PyObject* {

if(PyObject* obj = __pythran_wrap_DWBAintegrand0(self, args, kw))
    return obj;
PyErr_Clear();


if(PyObject* obj = __pythran_wrap_DWBAintegrand1(self, args, kw))
    return obj;
PyErr_Clear();


if(PyObject* obj = __pythran_wrap_DWBAintegrand2(self, args, kw))
    return obj;
PyErr_Clear();


if(PyObject* obj = __pythran_wrap_DWBAintegrand3(self, args, kw))
    return obj;
PyErr_Clear();

                return pythonic::python::raise_invalid_argument(
                               "DWBAintegrand", "\n    - DWBAintegrand(float, float64[:], float64[:,:], float64[:,:], float64[:], float64[:], float64[:], float, float, float, float)", args, kw);
                });
            }


static PyMethodDef Methods[] = {
    {
    "DWBAintegrand",
    (PyCFunction)__pythran_wrapall_DWBAintegrand,
    METH_VARARGS | METH_KEYWORDS,
    "\nIntegration function. Needs to be simplifyed in the future to make it\nlow level function.\n\n\nSupported prototypes:\n\n- DWBAintegrand(float, float64[:], float64[:,:], float64[:,:], float64[:], float64[:], float64[:], float, float, float, float)"},
    {NULL, NULL, 0, NULL}
};


#if PY_MAJOR_VERSION >= 3
  static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "dwba",            /* m_name */
    "",         /* m_doc */
    -1,                  /* m_size */
    Methods,             /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#define PYTHRAN_RETURN return theModule
#define PYTHRAN_MODULE_INIT(s) PyInit_##s
#else
#define PYTHRAN_RETURN return
#define PYTHRAN_MODULE_INIT(s) init##s
#endif
PyMODINIT_FUNC
PYTHRAN_MODULE_INIT(dwba)(void)
#ifndef _WIN32
__attribute__ ((visibility("default")))
__attribute__ ((externally_visible))
#endif
;
PyMODINIT_FUNC
PYTHRAN_MODULE_INIT(dwba)(void) {
    import_array()
    #if PY_MAJOR_VERSION >= 3
    PyObject* theModule = PyModule_Create(&moduledef);
    #else
    PyObject* theModule = Py_InitModule3("dwba",
                                         Methods,
                                         ""
    );
    #endif
    if(! theModule)
        PYTHRAN_RETURN;
    PyObject * theDoc = Py_BuildValue("(sss)",
                                      "0.9.2",
                                      "2019-07-04 19:06:34.227565",
                                      "8fb978b650b9256e72a79f238fccfa34b2a6cb7cdb2ce987602443f29a60c023");
    if(! theDoc)
        PYTHRAN_RETURN;
    PyModule_AddObject(theModule,
                       "__pythran__",
                       theDoc);

    PyModule_AddObject(theModule, "__transonic__", __transonic__);
    PYTHRAN_RETURN;
}

#endif