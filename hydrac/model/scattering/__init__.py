"""
Scattering
==========

Scattering package computes the scattering models (form function, total
scattering cross section, ...) based on the model presets defined by the
user.

Below is a quick introduction to the concept of scattering models. More
details can be found in Medwin & Clay 1997 and Morse & Ingaard 1987.

In the current problem, we consider an acoustic pressure field of amplitude
:math:`P_{i}` emitted by a source, and propagating away from that source. 
In presence of an inhomogeniety such as a particle, the acoustic wave will 
be scattered in all directions. The amplitude of the scattered pressure
field is :math:`P_{s}`. Considering the particle as the source
for the scattered wave,:math:`P_{s}` can be expressed in terms of
:math:`P_{i}`.

At large distance from a source, the amplitude of an acoustic wave
decreases as :math:`\\frac{1}{r}` by spherical spreading, and
:math:`10^{-\\frac{\\alpha_{w}r}{20}}` by water absorption. The amplitude
of the scattered wave thus writes :

.. math:: P_s = \\frac{1}{r}P_{i}|L(\\theta,\\phi,f)|10^{-\\frac{\\alpha_{w}r}{20}}

with

.. math:: |L(\\theta,\\phi,f)|^2=\\Delta_s(\\theta,\\phi,f)

where :math:`|L(\\theta,\\phi,f)|` and :math:`\\Delta_s(\\theta,\\phi,f)`
are called the scattering length (in m) and differential cross-section
(in m\ :sup:`2`\), and represents the amount of energy scattered by the
particle in all space compared to the incident energy
(:math:`\\theta` and :math:`\\phi` are respectively the polar and colatitude angles
in spherical coordinate system). :math:`\\sigma_{bs}=\\Delta_s(0,0,f)` is
called the backscattering cross-section (differential cross section evaluated
in the backscattered direction). Other useful and widespread notations are the
form function :math:`f=\\frac{2}{a}\sqrt(\\sigma_{bs})` and the total
scattering cross section :math:`\\chi` is the integral of
:math:`\\Delta_s(\\theta,\\phi,f)` over all space. 

These complex functions vary with the size and shape of the particle, 
the carrier frequency of the acoustic wave, the angle of orientation in the
incident pressure field, or the acoustic impedence ratios between the
inhomogeneity and surrounding fluid (density ratio, compressibility ratio).
They decribe the scattering properties for one single particle. Most of the
time, they are made function of the wave number and the characteristic
length of the particle (ex. radius for a sphere, semi-minor axis for a prolate
spheroid...) :math:`ka`.

Using active acoustic instruments the pressure amplitude backscattered by
all the particles present in the insonified volume can be measured. In this
sense, modeling these functions is of utmost importance for the
interpretation of the backscatter pressure/intensity.

These function do possess exact analytical solution for canonical shapes
such as sphere and cylinders (Medwin & Clay 1997). But when considering
arbitrarily or irregularly shaped particles (ex. zooplankton), the
backscattering cross-section modeling becomes intrincate and several
approximations need to be made, leading to new theoretical formulations 
(ex. Stanton et al. 1998) or empirical-based models (ex. Thorne & Hanes 2002).

Below is an example of the scattering length for a rigid sphere for
different ka values, normalized by the root mean square of the geometric cross
section of the sphere (:math:`\sqrt(\pi a^2)`):

.. image:: ../_image/scatt.pdf
   :width: 85%
   :align: center

Here is a plot containing multiple backscatter models that can be computed 
using the present package. The complete list of models that can be computed
with hydrac is given in the package :mod:`hydrac.model.particle`, along
with the useful bibliography:
    
.. image:: ../_image/model.pdf
   :width: 70%
   :align: center

The package contains several model generation schemes : empirical, theoretical,
arbitrary integral models... The following tools allow the computation of a
fairly wide variety of scattering models routinely used in fisheries acoustics
and sediment transport monitoring :

.. autosummary::
   :toctree:

   scattering
   dwba
   emp
   thq
   hp

"""

#Si le fichier __init__.py du paquet définit une liste nommée __all__, cette liste sera utilisée comme liste des noms de modules devant être importés lorsque from package import * est utilisé. C’est le rôle de l’auteur du paquet de maintenir cette liste à jour lorsque de nouvelles version du paquet sont publiées.

from ..scattering import scattering
from ..scattering import thq
#from .acoustique import acoustic
#from .aquascat import *


__all__ = ['scattering', 'thq']
