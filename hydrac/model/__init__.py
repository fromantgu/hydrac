"""
Models
======

Model package containing several scattering models and water parameters tools.
The package contain user oriented model preset tools for setting the parameters
necessary calculate the correct desired model :

.. autosummary::
   :toctree:

   model
   particle
   water
   
And a subpackage that actually computes the the form function and total
scattering cross section given the information provided by the model presets :

.. autosummary::
   :toctree:

   hydrac.model.scattering

"""

#Si le fichier __init__.py du paquet définit une liste nommée __all__, cette liste sera utilisée comme liste des noms de modules devant être importés lorsque from package import * est utilisé. C’est le rôle de l’auteur du paquet de maintenir cette liste à jour lorsque de nouvelles version du paquet sont publiées.

from ..model import model
from ..model import particle
from ..model import water
from ..model import scattering
#from .acoustique import acoustic
#from .aquascat import *

__all__ = ['model', 'water', 'particle']
