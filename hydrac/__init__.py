"""
Hydrac
======

Provides 

  1. An object-oriented and modular set of solvers.
  2. Tools useful for carrying out acoustic inversions using different types of datasets.

The docstring examples assume that `hydrac` has been imported as `hdc`::

  >>> import hydrac as hdc

Use the built-in ``help`` function to view a function's docstring::

  >>> help(hdc.figs.Figures)
  ... # doctest: +SKIP

Available subpackages
---------------------
…

"""

from hydrac.util import constants
from hydrac import calcul
from hydrac.util import parameters

from hydrac import instruments
from hydrac import model
from hydrac import inversion

import numpy as np

#from hydrac.Instruments import aquascat
#from hydrac.Instruments import instruments
#from hydrac.Instruments import acoustique
#from hydrac.Instruments import aquascat
#from hydrac import Instruments
#From Instruments import * 
