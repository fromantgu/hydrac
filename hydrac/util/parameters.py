"""
Parameters class (:mod:`hydrac.util.parameters`)
================================================

.. autoclass:: AttrDict
   :members:
   :private-members:


"""


# import numpy as np
from hydrac.util.paramcontainer import ParamContainer
from copy import copy


class AttrDict(dict):
    """ AttrDict class.
    This class transforms any type of data structure to a dictionary with
    attribute-style access. It maps attribute access to the real dictionary.
    This class can handle lists of lists, dictionnaries, tuples and
    ParamContainers from the package :mod:`hydrac.util.paramcontainer`
    """
    def __init__(self, inpt={}):
        super().__init__(inpt)

    def __dir__(self):
        return super().__dir__() + [str(k) for k in self.keys()]

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __bool__(self):
        return self.__len__() > 0

    __nonzero__ = __bool__

    def __setattr__(self, name, value):
        self[name] = value

    def __rename__(self, old, new):
        self[new] = self.pop(old)

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __getitem__(self, name):
        if name in self:
            return dict.__getitem__(self, name)
        else:
            raise AttributeError("No such attribute: " + name)

    def __setitem__(self, name, value):
        dict.__setitem__(self, name, value)

    def update(self, *args, **kwargs):
        for k, v in dict(*args, **kwargs).items():
            self.__setattr__(k, v)

    def update_param(self):
        print("updating list")
        return self.parse_dict(self)

    def traverse(self, dic, path=None):
        if not path:
            path = []
        if isinstance(dic, dict):
            for x in dic.keys():
                local_path = path[:]
                local_path.append(x)
                for b in self.traverse(dic[x], local_path):
                    yield b
        else:
            yield path, dic

    def find_path(self, key):

        return [x for x in self.traverse(self) if x[0][-1] == key]

    def display(self):
        print('blabla')

    def move_to_other_dict_and_delete_item(self, P_i, P_s, k):
        P_s[k] = copy(P_i[k])
        P_i.__delattr__(k)

    def parse_dict(self, dico):

            if isinstance(dico, dict):
                dico = AttrDict(dico)
                for tutu, titi in enumerate(dico):
                    if isinstance(dico[titi], dict):
                        dico[titi] = AttrDict(dico[titi])
                        print(('{} is now Parameters').format(titi))
                        dico[titi] = self.parse_dict(dico[titi])

                return dico
            else:
                print(('{} is not a dictionary').format(self))

                return dico


def Parameters(dico):

    """Walks a simple data structure, converting dictionary to AttrDict.
    Supports lists, tuples, and dictionaries containing dictionaries.
    """
    if isinstance(dico, dict):
        return AttrDict(dict((str(k), Parameters(v)) for
                             (k, v) in dico.items()))
    elif isinstance(dico, list):
        return list(Parameters(i) for i in dico)
    elif isinstance(dico, tuple):
        return tuple(Parameters(i) for i in dico)
    elif isinstance(dico, (ParamContainer)):
        x = lambda d: d if (Parameters(d) != {} and
                            hasattr(d, '_value_text') is False)\
                        or (Parameters(d) == {} and
                            '_value_text' not in d.__dict__) else d._value_text
        ll = list(dico.__dict__)
        ll2 = list(dico.__dict__)
        [ll2.remove(a) for a in ll if type(a) != str or a[0] == '_']

        return AttrDict(dict((i, Parameters(x(dico[i]))) for i in ll2))
    else:
        return dico
