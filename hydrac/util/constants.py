"""
Constants (:mod:`hydrac.util.constants`)
==========================================


g = 9.81 m/s^2

nu_pure_water = 1.e-6 m^2/s

rho0 = 1. kg/m^3

rho_pure_water = 0.998 kg/m^3


"""

import numpy as np
from pathlib import Path as pt

g = 9.81 # (m/s^2)
nu_pure_water = 1.e-6 # (m^2/s)
rho0 = 1. # (kg/m^3)
rho_pure_water = 0.998 # (kg/m^3)

list_instrus=['Aquascat', 'ADCP', 'ACVP']
#model_presets={'EMP':'Thorne','THQ':'TFS','THQ':'PMMA'}
#model_presets={'Thorne2008':{'mtype':'EMP',
#                             'nature':'MIN',
#                             'shape':'SPH',
#                             'default':{'cs':3760,
#                                        'cp':5980,
#                                        'rhos':2650,
#                                        'mode_num':0}},
#               'TFS':{'mtype':'THQ',
#                      'nature':'ORG',
#                      'shape':'SPH',
#                      'default':{'cs':0,
#                                 'cp':1600,
#                                 'rhos':1200,
#                                 'mode_num':2}},
#               'PMMA':{'mtype':'THQ',
#                       'nature':'ELA',
#                       'shape':'SPH',
#                       'default':{'cs':1340,
#                                  'cp':2690,
#                                  'rhos':1190,
#                                  'mode_num':20}}}
#
#dir_models_presets='hydrac/util/models_presets/'
#dir_aqa_calib='hydrac/util/aqa/'
#dir_adcp_calib='hydrac/util/adcp/'
#dir_ubmes_calib='hydrac/util/ub_mes/'

currpath=str(pt(__file__).parent)
dir_models_presets=currpath + '/models_presets/'
dir_aqa_calib=currpath + '/aqa/'
dir_ubsed_calib=currpath + '/ubsed/'
dir_adcp_calib=currpath + '/adcp/'
dir_ubmes_calib=currpath + '/ub_mes/'
dir_peacock_calib=currpath + '/peacock/'

water_custom={'P':1,'S':23,'T':12}

ADCP={'RDI':{'f300':{'FrequencyNominal':300000,
                    'Frequency':300000,
                    'TransducerRadius':0.079/2,
                    'TransducerBeamWidth':2.87,
                    'TransducerAngle':20,
                    'Kc':0.42,
                    'EC0':45,
                    'B':70,
                    'SL0':217},
               'f600':{'FrequencyNominal':600000,
                      'Frequency':614400,
                      'TransducerRadius':0.0762/2,
                      'TransducerBeamWidth':3,
                      'TransducerAngle':20,
                      'Kc':0.42,
                      'EC0':45,
                      'B':70,
                      'SL0':217},
               'f1200':{'FrequencyNominal':1200000,
                       'Frequency':1228800,
                       'TransducerRadius':0.051/2,
                       'TransducerBeamWidth':0.99,
                       'TransducerAngle':20,
                       'Kc':0.42,
                       'EC0':45,
                       'B':70,
                       'SL0':217}}}

CalibAQA={'SN-910-116':{ 'Kt':[(1e6,0.0416000000000000),
                               (2e6,0.0139900000000000),
                               (4e6,0.0101400000000000),
                               (0.5e6,0.0453500000000000)],
                         'cal_depth1':{'a':20.0777,'b':339.3548,'d':'<0'}},
          'SN-910-166':{ 'Kt':[(0.3e6, 0.37581),
                               (0.5e6, 0.02041),
                               (1e6, 0.02639),
                               (2e6, 0.00971),
                               (4e6, 0.01193)],
                         'cal_depth1':{'a':164.2845,'b':514.8679,'d':'>0'}}}