"""
Inversion
=========

Inversion package containing several inversion methods.

.. autosummary::
   :toctree:

   inversion
   run_inversion_attenuation
   run_inversion_implicit
   run_inversion_NNLS
   
Inversion methods will differ given the king of hydroacoustic data, namely, if
they are single frequency or multifrequency. The following two sub-packages
contain tools to feed the acoustic inversion in either case :

.. autosummary::
   :toctree:

   hydrac.inversion.multifrequence
   hydrac.inversion.monofrequence

   
"""

from ..inversion import inversion
from ..inversion import run_inversion_attenuation
from ..inversion import run_inversion_NNLS
from ..inversion import run_inversion_implicit