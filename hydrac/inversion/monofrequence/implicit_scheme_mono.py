#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Single Frequency Implicit scheme (:mod:`hydrac.inversion.singlefrequence.implicit_scheme_mono`)
===============================================================================================

"""

import numpy as np


def implicit_scheme_mono(asv_slice, a0, r, model, dr=None, nearfield_min=0):

    if 'rhoex' in model.particle.param.keys():
        print('Floc model used for inversion')
        rhos = model.particle.param.rhoex[np.where(np.abs(
                model.scattering.a_HR - a0) == np.min(np.abs(
                        model.scattering.a_HR - a0)))[0][0]] + model.cw
    else:
        rhos = model.particle.param.rhos

    if 'f_0HR' not in model.scattering.keys():
        As = (3 / (4 * rhos)) * model.scattering.f_infHR.T[np.where(np.abs(
                model.scattering.a_HR - a0) == np.min(np.abs(
                        model.scattering.a_HR - a0)))[0][0], :]**2 / a0
        zeta = (3 / (4 * rhos)) * model.scattering.c_infHR.T[np.where(np.abs(
                model.scattering.a_HR - a0) == np.min(np.abs(
                        model.scattering.a_HR - a0)))[0][0], :] / a0
    else:
        As = (3 / (4 * rhos)) * model.scattering.f_0HR.T[np.where(np.abs(
                model.scattering.a0_HR - a0) == np.min(np.abs(
                        model.scattering.a0_HR - a0)))[0][0], :]**2 / a0
        zeta = (3 / (4 * rhos))*model.scattering.c_0HR.T[np.where(np.abs(
                model.scattering.a0_HR - a0) == np.min(np.abs(
                        model.scattering.a0_HR - a0)))[0][0], :] / a0

    if len(As) == 1 and np.size(asv_slice, 1) > 1:
        As2 = list(As)
        zeta2 = list(zeta)
        [As2.append(As[0]) for i in range(1, np.size(asv_slice, 1))]
        As = np.array(As2)
        [zeta2.append(zeta[0]) for i in range(1, np.size(asv_slice, 1))]
        zeta = np.array(zeta2)

    Jb = np.array([a / b for a, b in zip(asv_slice.T, As)]).T
    conc = np.zeros(np.shape(Jb))
    atten = np.zeros(np.shape(Jb))
    conc[0, :] = conc[0, :] + Jb[0, :]
    atten[0, :] = conc[0, :] * zeta * r[0]
    for i in range(0, np.size(Jb, 0)-1):
        conc[i+1, :] = conc[i, :] * Jb[i+1, :] / Jb[i, :] *\
            np.exp(2 * dr[i] * conc[i, :] * zeta)
        atten[i+1, :] = atten[i, :] + dr[i] * conc[i, :] * zeta

    return conc, atten
