"""
Singlefrequency
===============

Inversion package for singlefrequency applicatinos.

.. autosummary::
   :toctree:

   implicit_scheme_mono
  
"""
from ..monofrequence import implicit_scheme_mono
