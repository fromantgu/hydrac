#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Attenuation method (:mod:`hydrac.inversion.attenuation_scheme`)
===============================================================

"""

import numpy as np
# import scipy as sc
from copy import deepcopy
from hydrac.util.parameters import Parameters


def implicit_scheme_mono(asv_slice, r, model, dr=None, nearfield_min=0):
    return