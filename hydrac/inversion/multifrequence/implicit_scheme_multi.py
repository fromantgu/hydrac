#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multifrequency Implicit scheme (:mod:`hydrac.inversion.multifrequence.implicit_scheme_multi`)
=============================================================================================

"""

import numpy as np
from copy import deepcopy


def implicit_scheme_multi(asv_slice, r, model, dr=None, nearfield_min=0):

    s_prim = np.size(asv_slice, 1)
    s_prim_ = np.size(asv_slice, 1)
    n_prim = s_prim_
    l_ = []
    cnt = 0
    while s_prim_ > 0:
        s_prim_ -= 1
        for t in range(cnt, s_prim):
            l_.append([cnt, t])
        n_prim = n_prim + (s_prim_ - 1)
        cnt += 1
    [l_.remove([a, a]) for a in range(0, cnt) if [a, a] in l_]
    ratioSV = []
    ratioModel = []
    for i, j in l_:
        ratioSV.append(asv_slice[:, i] / asv_slice[:, j])
        ratioModel.append(model.scattering.f_infHR[i, :] ** 2 /
                          model.scattering.f_infHR[j, :] ** 2)

    if dr is None:
        dr = np.diff(r)
    dr_tot = 0
    cnt = 0
    while dr_tot <= nearfield_min:
        cnt += 1
        dr_tot += dr[cnt - 1]

    ind_debut = cnt - 1
    freq_ref_ind = np.where(np.abs(np.array(model.scattering.freq) - 1e6) ==
                            min(np.abs(np.array(model.scattering.freq) - 1e6))
                            )[0][0]

    Jbon = deepcopy(asv_slice)
    ratioSV = np.array(ratioSV)
    lr = np.size(asv_slice, 0)
    a_sol = np.zeros(np.shape(asv_slice)[0])
    M1 = np.zeros(np.shape(asv_slice)[0])

    atten = np.zeros((np.size(asv_slice, 1), lr))
    eta = np.zeros((np.size(asv_slice, 1), lr))

    ao, atot = find_a0(ratioSV[:, ind_debut], ratioModel, model)
    a_sol[0] = ao

    _ind_model = np.where(np.abs(ao - model.scattering.a0_HR) ==
                          np.min(np.abs(ao - model.scattering.a0_HR)))[0][0]

    if 'rhoex' in model.particle.param.keys():
        print('Floc model used for inversion')
        rhos = model.particle.param.rhoex[np.where(np.abs(
                model.scattering.a_HR - ao) == np.min(np.abs(
                        model.scattering.a_HR - ao)))[0][0]] + model.cw
    else:
        rhos = model.particle.param.rhos

    ks = model.scattering.f_0HR[freq_ref_ind, _ind_model] / np.sqrt(rhos * ao)
    eta = eta + np.tile(3 * model.scattering.c_0HR[:, _ind_model] /
                        (4 * rhos * ao), (lr, 1)).T

    # initial mass eqn (4) in the paper with the nearfield correction included
    M1a = np.squeeze((16 * np.pi / (3 * ks ** 2)) * Jbon[:, freq_ref_ind])
    eta[:, 0:ind_debut] = 0
    # iterations to account for attenuation equations (6) and (7) in the paper
    for n in range(ind_debut, lr-1):

        if n > ind_debut:
            o_ks = ks
            for hu in range(np.size(ratioSV, 0)):
                ratioSV[hu, n] = ratioSV[hu, n] *\
                    np.exp(atten[l_[hu][0], n-1] - atten[l_[hu][1], n-1])

            ao, atot = find_a0(ratioSV[:, n], ratioModel, model)
            
            if np.isnan(ao):
                a_sol[n]=np.nan
                M1a[n]=np.nan
            else:
                _ind_model = np.where(np.abs(ao - model.scattering.a0_HR) ==
                          np.min(np.abs(ao - model.scattering.a0_HR)))[0][0]
            
                a_sol[n] = ao

                if 'rhoex' in model.particle.param.keys():
                    try:
                        idx_ao = np.where(np.abs(model.scattering.a_HR - ao) ==
                                          np.min(np.abs(model.scattering.a_HR - ao
                                                        )))[0][0]
                        rhos = model.particle.param.rhoex[idx_ao] + model.cw
                    except(IndexError):
                        rhos = np.NaN
    
                else:
                    rhos = model.particle.param.rhos
#                print('kjnkjbnkbn {}'.format(_ind_model))
                ks = model.scattering.f_0HR[freq_ref_ind, _ind_model] / np.sqrt(
                        rhos * ao)
                eta[:, n] = 3 * model.scattering.c_0HR[:, _ind_model
                                                       ] / (4 * rhos * ao)
                M1a[n] = M1a[n] * o_ks ** 2 / ks ** 2

        M1[n] = M1a[n]

        for loop in range(0, 50):
            Mtemp1 = M1[n]
            atten[:, n] = np.sum(eta[:, 0:n+1] * np.tile(M1[0:n+1] * dr[0:n+1],
                                                         (np.size(asv_slice, 1
                                                                  ), 1)), 1)
            M1[n] = M1a[n] * np.exp(4 * r[n] * atten[freq_ref_ind, n])

            if np.abs((M1[n] - Mtemp1) / M1[n]) < 0.0001:
                break
    return M1, a_sol, atten


def find_a0(r_sv, r_m, model):
    a_min = []
    for u in range(len(r_sv)):
        _tmp = model.scattering.a0_HR[np.where(np.abs(r_m[u] - r_sv[u]) ==
                                               np.min(np.abs(r_m[u] - r_sv[u])
                                                      ))]
        if np.size(_tmp) > 0\
                and np.size(_tmp) < np.size(model.scattering.a0_HR) / 2:
            a_min.append(_tmp)

    a_min = reject_outliers(np.array(a_min), m=2)
    return np.nanmean(a_min), a_min


def reject_outliers(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.
    return data[s < m]
