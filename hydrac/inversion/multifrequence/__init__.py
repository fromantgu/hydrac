"""
Multifrequency
==============

Inversion package for multifrequency applicatinos.

.. autosummary::
   :toctree:

   attenuation_scheme
   implicit_scheme_multi
   nnls
   
  
"""

from ..multifrequence import attenuation_scheme
from ..multifrequence import implicit_scheme_multi
from ..multifrequence import nnls
