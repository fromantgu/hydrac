#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Attenuation scheme (:mod:`hydrac.inversion.run_inversion_attenuation`)
======================================================================

.. autoclass:: Run_inversion_attenuation
   :members:
   :private-members:

"""

import numpy as np
from .inversion import Inversion

from hydrac.util.parameters import Parameters
#from .calcul import Resample
class Run_inversion_attenuation(object):
    """Attenuation based inversion scheme class

    This method uses the frequency dependency of the scattering attenuation,
    function of the total scattering cross-section, and is descibed in Hurther
    et al. 2011 (doi:10.1016/j.coastaleng.2011.01.006). This inversion method 
    can work for multifrequency observations only (minimum 2 frequencies).


    Parameters
    ----------

    obj :

      Hydroacoustic object defined by
      :class:`hydrac.instruments.acoustique.Acoustic`.

    """
#     
    def __init__(self,obj):
        pass
#        Inversion.__init__(self,o_hac,o_scatt,method=None)
##        self.BX=BX
##        
##        _dim=np.size(BX,1)
#        self.o_hac=o_hac
#        self.o_scatt=o_scatt
#        self.kwargs=kwargs
#        
#        if 'depr' in self.kwargs:
#            self.o_hac.hac_data_decimation()
##        self._dim=np.size(self.obj.
#        
#        
##        self.dim_of_interest=np.where(np.array(np.shape(BX)))
#                          
##        Ici on utilise l'héritage de la classe Inversion sur modèle de scattering.thq
#        if self.o_hac.tag_M == 'multi':
#            self.calc_inv(self.elementary_inv, self.o_hac.param)
#        else :
#            return print('Impossible to invert the data with NNLS method')
#
#    def elementary_inv(self,BX):
#        
#        BX[np.where(np.logical_or(10*np.log10(BX)<-140, 10*np.log10(BX)>-20))]=np.NaN
#        
#        sfactr = 1e-6
#        
#        for ix in range(0,np.size(BX,0)):
#            
#            tmp=BX[ix,:,:]
#            tmp2=np.multiply(tmp,1/tmp)
#            inv_tag=np.isfinite(tmp)
#            a,b=np.shape(self.o_scatt.scattering.aij)  #to be corrected when switch to models for each time stamp/depth
#            Cinit=[]
#            anorm=[]
#            [Cinit.append(np.multiply(self.o_scatt.scattering.aij.T,np.tile(np.atleast_2d(np.array(1/oo)),(np.size(self.o_scatt.scattering.aij,1),1)))) for oo in list(tmp)]
#            [anorm.append(np.sqrt(np.nansum(np.nansum(np.multiply(o,o)))/(a*b))) for o in Cinit]
#                
#            tmp2=np.concatenate((tmp2,np.zeros((np.size(tmp2,0),b))),axis=1)
#            
#            taij=[]
#            [taij.append(np.concatenate((op1,op2*sfactr*np.eye(b,b)),axis=1)) for op1,op2 in zip(Cinit,anorm)]
#            
#            inv=[NNLS(mat.T,np.atleast_2d(obs).T,display='none') for mat,obs in zip(taij,tmp2)]
#            
#            norm=[]
#            [norm.append(op1[0:b,0:a].T@op2.solution[0:b]-np.atleast_2d(op3[0:a]).T) for op1,op2,op3 in zip(taij,inv,tmp2)]
#            rnorm=np.linalg.norm(norm,axis=1)
#            xnorm=[np.linalg.norm(o.solution) for o in inv]
#            
#            [o.__setattr__('bv', (4*np.pi/3) * np.multiply(np.power(self.o_scatt.scattering.a_UND,3).T,o.solution)) for o in inv]
#           

        
        
#        return inv #vecteur de taille [size(asv,1)]