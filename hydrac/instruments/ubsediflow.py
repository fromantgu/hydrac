#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Aquascat instruments (:mod:`hydrac.instruments.aquascat`)
=========================================================

.. autoclass:: Aquascat
   :members:
   :private-members:

"""

# from .instruments import instrument
from .acoustique import Acoustic
from hydrac.util import constants
import numpy as np
import struct as st
import glob
import os
import datetime
from tkinter import filedialog
from tkinter import Tk
from hydrac.util.parameters import Parameters
from hydrac.util.paramcontainer import ParamContainer
from hydrac.util.query import query_yes_no, query_number
from hydrac.util.udt3C_extractor.raw_extract import raw_extract



class UbSediFlow(Acoustic):
    """ Aquascat instrument class.

    Base class : :mod:`hydrac.instruments.acoustique.Acoustic`

    The Aquascat is a multifrequency profiler working at the kHz-MHz frequency
    range developpe by Aquatec Group. Its full description is given in
    http://www.aquatecgroup.com/11-products/78-aquascat. The example of a
    Aquascat 1000R is shown below :

    .. image:: ../_image/aqa.png
       :width: 50%
       :align: center

    The Aquascat class reads the raw binary files from Aquascat instruments
    and stores the valuable information into the ``param`` modified dictionnary
    with the common shape handled by hydrac
    (see :mod:`hydrac.instruments.acoustique`).

    A typical example of how should the data considered is as follows ::

      >>> A = Aquascat('Campaign_1')

    The above line will prompt the user to select one or multiple files in a
    directory, read and store each file data into ``param``
    instanciated in :class: `hydrac.instruments.acoustique.Acoustic`, as
    separate modified dictionnaries ``PX``, X being the file number. Ex: for
    the first file loaded in ``param``, one can look at the differents
    variables stored in ``param.P0`` ::

      >>> A.param.P0.keys()
      dict_keys(['BurstTime', 'PingRate', 'NumPingsTot',
      'NumChannels', 'NumProfilesSamples', 'NumAverage'...])

    One also gets the base Acoustic class instance ``instr_type``::

      >>> A.instr_type
      'acoustic'

    Or methods::

      >>> A.preproc_acoustic_data()

    which will prompt the user for extra information necessary to the
    processing of the hydroacoutic data such as water temperature. See
    :func:`hydrac.instruments.acoustique.Acoustic.preproc_acoustic_data`

    Parameters
    ----------

    name : str, {'Campaign_1'}

      Tag name, for personal usage, for instance to add a campaign name."""

    def __init__(self, name):
        Acoustic.__init__(self)
        self.instr_name = 'ubsed'
        self.name = name
        
        self.nearfield_min = 0.2
        self.filepath, self.tempdir = self.file_select()
        self.batch_read()
        if len(np.unique(self.param.P0.Frequency))==1:
            self.tag_M = 'mono'
        else:
            self.tag_M = 'multi'
    def file_select(self):
        """ User input selection of the target files to read """
        root = Tk()
        filez = filedialog.askopenfilenames(parent=root, title='Choose a file')
        root.withdraw()  # use to hide tkinter window

        filepath = list(filez)
        filepath.sort()
        filepath = tuple(filepath)
        tempdir = os.path.dirname(os.path.abspath(filez[0]))
        return filepath, tempdir
    
    def export_matlab(self,filename_out):
        import scipy as sc
        mdic={}
        list_tmp_fname=[]
        for ko in range(len(self.filepath)):
            list_tmp_fname.append(self.filepath[ko].split('/')[-1])
        for ko in range(len(self.param)):
            mdic.update({"P"+str(ko):{}})
            mdic["P"+str(ko)].update({"int":self.param["P"+str(ko)].RawIntensity,
                                      "dist":self.param["P"+str(ko)].BinRange,
                                      "t":self.param["P"+str(ko)].time,
                                      "Frequency":self.param["P"+str(ko)].Frequency,
                                      "PingRate":self.param["P"+str(ko)].PingRate,
                                      "PulseLength":self.param["P"+str(ko)].PulseLength,
                                      "vel":self.param["P"+str(ko)].ub_sed.vel_avg,
                                      "vel_std":self.param["P"+str(ko)].ub_sed.vel_std,
                                      "sat":self.param["P"+str(ko)].ub_sed.echo_sat,
                                      "snr":self.param["P"+str(ko)].ub_sed.snr,
                                      "fname":list_tmp_fname[ko]})
        sc.io.savemat(filename_out+".mat",mdic)

    def batch_read(self):
        """Read a list of udt raw UB_MES file using the method :func:`read_UB`
        """

        for kk in range(len(self.filepath)):
            self.currfilepath = self.filepath[kk]
            # print('toto est {}'.format(len(self.filepath[kk])))
            # toto=self.filepath[kk]
            # print('toto est {}'.format(toto))

            # toto2=self.read_UB(toto)
            # print('toto2 est {}'.format(toto))

            # tmp = self.param_shape_UB(toto2)
            tmp = self.param_shape_UB(self.read_UB(self.filepath[kk]))
            self.param.update({"P"+str(kk): tmp})
            
            # En bistatique, Kt est diff pour chaque porte. Implémenté ici
            # directement sur les Raw int.

            del self.currfilepath
    
    def read_UB(self,fname):
        """Read one .udt raw Aquascat file"""


        (
            device_name,
            time_begin,
            time_end,
            param_us_dicts,
            data_us_dicts,
            data_dicts,
            settings_dict
        ) = raw_extract(fname)
        
        
        return Parameters({'device_name': device_name,
                           'time_begin': time_begin,
                           'time_end': time_end, 'param_us_dicts': param_us_dicts, 'data_us_dicts': data_us_dicts,
                           'data_dicts': data_dicts})
     
    def param_shape_UB(self, inp):
        """Reshapes the modified dictionnary ``param`` to fit the standard key
        names and data shape used throughout hydrac"""

        inp2 = Parameters({'ub_sed': {}})
        inp3 = Parameters({})

        inp2.BurstTime = datetime.datetime.timestamp(inp.time_begin)
        inp2.PingRate = inp.param_us_dicts['1']['1']['prf']
        
        inp2.NumChannels = len(inp.param_us_dicts)
        
        pUS=Parameters({})
        [pUS.update({'pUS'+str(i+1):inp.param_us_dicts[str(i+1)][list(inp.param_us_dicts[str(i+1)].items())[0][0]]}) for i in range(inp2.NumChannels)]
        dUS=Parameters({})
        [dUS.update({'dUS'+str(i+1):inp.data_us_dicts[str(i+1)][list(inp.data_us_dicts[str(i+1)].items())[0][0]]}) for i in range(inp2.NumChannels)]

        dec=[]
        
        [dec.append(len(dUS['dUS' +str(i+1)]['echo_avg_profile']['time'])) for i in range(inp2.NumChannels)]

        dec_it=np.min(dec)
        dec_id=np.where(dec==dec_it)[0][0]
        inp2.NumProfilesSamples = np.ones((inp2.NumChannels)) *\
            np.float64(dec_it)
        inp2.NumAverage = np.ones((inp2.NumChannels)) * pUS.pUS1.n_avg * pUS.pUS1.n_p
        inp2.SampleRate = np.ones((inp2.NumChannels)) * inp2.PingRate / inp2.NumAverage
        
            
        inp2.BinLengthMM =  np.squeeze(np.zeros((1,inp2.NumChannels)))
        inp2.StartBin = np.squeeze(np.zeros((1,inp2.NumChannels)))
        inp2.NumBins = np.squeeze(np.zeros((1,inp2.NumChannels)))
        for i in range(inp2.NumChannels):
            inp2.NumBins[i]=pUS['pUS'+str(i+1)]['n_cell']
       
        NumBins=int(np.min(inp2.NumBins))
        for i in range(inp2.NumChannels):
            inp2.Frequency = np.squeeze(np.zeros((1,inp2.NumChannels)))
            inp2.FrequencyRx = np.squeeze(np.zeros((1,inp2.NumChannels)))
            inp2.RxGain = np.squeeze(np.zeros((1,inp2.NumChannels)))
            inp2.TxGain = np.squeeze(np.zeros((1,inp2.NumChannels)))
            print('Numbins = {}'.format(np.zeros((1,inp2.NumChannels))))
            inp2.RawIntensity = np.zeros((int(NumBins),int(inp2.NumProfilesSamples[0]),int(inp2.NumChannels)))
            inp2.BinRange=np.zeros((int(NumBins),int(inp2.NumChannels)))
            
            inp3.echo_sat=np.zeros((int(NumBins),int(inp2.NumProfilesSamples[0]),int(inp2.NumChannels)))
            inp3.vel_avg=np.zeros((int(NumBins),int(inp2.NumProfilesSamples[0]),int(inp2.NumChannels)))
            inp3.vel_std=np.zeros((int(NumBins),int(inp2.NumProfilesSamples[0]),int(inp2.NumChannels)))
            inp3.snr=np.zeros((int(NumBins),int(inp2.NumProfilesSamples[0]),int(inp2.NumChannels)))
            inp3.TVGa0=np.zeros((dec_it,int(inp2.NumChannels)))
            inp3.TVGa1=np.zeros((dec_it,int(inp2.NumChannels)))

        
        
        for i in range(inp2.NumChannels):
            inp2.BinLengthMM[i]=pUS['pUS'+str(i+1)]['r_em']*1000
            inp2.StartBin[i]=pUS['pUS'+str(i+1)]['r_cell1']
            
            inp2.Frequency[i]=pUS['pUS'+str(i+1)]['f0']
            inp2.FrequencyRx[i]=pUS['pUS'+str(i+1)]['f0']   
            inp2.TxGain[i]=pUS['pUS'+str(i+1)]['a0'] 
            inp2.RawIntensity[:,:,i]=np.array(dUS['dUS'+str(i+1)]['echo_avg_profile']['data'])[0:dec_it,0:NumBins].T
            inp2.BinRange[:,i]=inp2.StartBin[i]+(np.arange(0,NumBins))*inp2.BinLengthMM[i]/1000
            # inp3.echo_sat[:,:,i]=np.array(dUS['dUS'+str(i+1)]['echo_sat_profile']['data'])[0:dec_it,:].T
            inp3.echo_sat[:,:,i]=np.array(dUS['dUS'+str(i+1)]['saturation_avg_profile']['data'])[0:dec_it,0:NumBins].T
            inp3.vel_avg[:,:,i]=np.array(dUS['dUS'+str(i+1)]['velocity_avg_profile']['data'])[0:dec_it,0:NumBins].T
            inp3.vel_std[:,:,i]=np.array(dUS['dUS'+str(i+1)]['velocity_std_profile']['data'])[0:dec_it,0:NumBins].T
            inp3.snr[:,:,i]=np.array(dUS['dUS'+str(i+1)]['snr_doppler_avg_profile']['data'])[0:dec_it,0:NumBins].T
            inp3.TVGa0[:,i]=np.array(dUS['dUS'+str(i+1)]['a0_param']['data'][0:dec_it])
            inp3.TVGa1[:,i]=np.array(dUS['dUS'+str(i+1)]['a1_param']['data'][0:dec_it])
        # inp2.BinLengthMM = 1000 * np.array([inp.param_us_dicts['1']['1']['r_em'], inp.param_us_dicts['2']['2']['r_em']])
        # inp2.StartBin = np.array([inp.param_us_dicts['1']['1']['r_cell1'], inp.param_us_dicts['2']['2']['r_cell1']])
        # inp2.NumBins = np.array([inp.param_us_dicts['1']['1']['n_cell'], inp.param_us_dicts['2']['2']['n_cell']])
        
        inp2.TransducerTag = np.array(list(range((inp2.NumChannels))))
        inp2.TransducerRadius = np.zeros(np.shape(inp2.BinLengthMM))
        inp2.TransducerBeamWidth = np.zeros(np.shape(inp2.BinLengthMM))
        
        # inp2.Frequency = np.ones((inp2.NumChannels)) * tmp.f0 * 1e6
        # inp2.FrequencyRx = np.ones((inp2.NumChannels)) * tmp.f0 * 1e6
       
        inp2.NumPingsTot = inp2.NumAverage[0]*inp2.NumProfilesSamples[0]

        inp2.TransducerName = np.tile(np.array(str(inp2.Frequency) + ' MHz',
                                               dtype=str),
                                      (inp2.NumChannels))
        inp2.PulseLength = inp2.BinLengthMM * 2 / pUS.pUS1.sound_speed/1000
        
        inp2.time = []
        [inp2.time.append(datetime.datetime.timestamp(i)) for i in dUS['dUS' + str(dec_id+1)]['echo_avg_profile']['time']]
        inp2.time = np.array(inp2.time)
        inp2.TVG = np.tile(np.zeros((np.size(inp2.BinRange, 0), 1)),
                           (1, inp2.NumChannels))
                
        inp2.MeanProfile = np.nanmean(inp2.RawIntensity, 1)
        # inp2.BinRange = np.tile(inp.r, (1, inp2.NumChannels))
    #        inp2.BinRange_bj = np.tile(inp.bj, (1, inp2.NumChannels))
        inp2.SerialNum = str(inp.device_name[-6:])
        inp2.pressure = np.tile(np.zeros((dec_it, 1)),
                                (1, inp2.NumChannels))
        inp2.depth_raw = np.tile(np.zeros((dec_it, 1)),
                                 (1, inp2.NumChannels))
        inp2.depth = np.zeros((dec_it))
        inp2.temp = np.array(inp.data_dicts.temperature.data[0::2][0:dec_it])
        inp2.battery = np.zeros((dec_it)) * np.nan
        # inp2.TransducerAngle = np.array([90-tmp.gamma * 180 /
        #                                  np.pi, -(90 - tmp.gamma
        #                                           * 180 / np.pi)])
        inp2.sal = np.zeros((dec_it)) * np.nan
        inp2.NumSamples = inp2.NumProfilesSamples
        inp2.FileName = os.path.split(self.currfilepath)[-1]
        inp3.time_aux=inp2.time
        inp2.ub_sed = inp3
        
        
        inp2.calib = self._set_calib_ubsed()
        
        try:
            TransducerKt = inp2.calib['SN' + str(inp2.SerialNum)].Kt
            inp2.TransducerKt = []
            [inp2.TransducerKt.append([a, b]) for a, b in TransducerKt
             if a in inp2.FrequencyRx]
            print("calib ok")            
            
                
        except(AttributeError):
            pass
        ex = False
        if 'SN' + str(inp2.SerialNum) not in inp2.calib.keys():
            inp2.calib.update({'SN' + str(inp2.SerialNum): Parameters({})})
            ex = True
            if query_yes_no('No Kt values for AQA SN' + str(
                    inp2.SerialNum) + ' - Do you want to enter Kt manually ? '):
        
                Kt = list()
                for ui in range(len(inp2.FrequencyRx)):
                    Kt.append((inp2.FrequencyRx[ui],
                               query_number('Kt channel F = ' +
                                            str(inp2.FrequencyRx[ui]))))
                inp2.calib['SN' + str(inp2.SerialNum)].update({'Kt': Kt})
        
            else:
                Kt = []
                for ui in range(len(inp2.FrequencyRx)):
                    Kt.append((inp2.FrequencyRx[ui], 1))
                inp2.calib['SN' + str(inp2.SerialNum)].update({'Kt': Kt})
        
            
            TransducerKt = inp2.calib['SN' + str(inp2.SerialNum)].Kt
            inp2.TransducerKt = []
            [inp2.TransducerKt.append([a, b]) for a, b in TransducerKt
             if a in inp2.FrequencyRx]
            
            hu=query_yes_no('Do you wosh to add other transducer Kts ?')
            Fx=[]
            while hu is True:
                Fx.append((query_number('Choose a frequency ?'), query_number('Choose a Kt')))
                hu=query_yes_no('Add another channel ?')
        
            [inp2.calib['SN' + str(inp2.SerialNum)].Kt.append(ui) for ui in Fx]
            
            
        if ex:
            if query_yes_no('Do you wish to save current' +
                            ' instrument calibration parameters ?'):
                self._update_calib_ubsed('SN' + str(inp2.SerialNum),
                                       inp2.calib['SN' +
                                                  str(inp2.SerialNum)])
            else:
                print('je pas au bon endroit')    
        

        return inp2        
    
    def _set_calib_ubsed(self):
        """see :func:`hydrac.instruments.aquascat.Aquascat._set_calib_aqa`.
        In addition here, as working with a bistatic instrument, the Kt value
        will change range as depending on the product of the transducers
        beam patterns. The Kt is thus a refined array of Kts with respect to
        the range from the emitter, proper to a bistatic configuration. This
        vector can be interpolated at the ranges determined by the current
        settings (that can evolve given the pulse length, speed of sound in
        water...)
        """
        rr = glob.glob(constants.dir_ubsed_calib + '*.xml')
        rr.sort()
        
        
        try:
            ub = Parameters(ParamContainer(path_file=rr[-1]))
        except(IndexError, AttributeError):
            ub = Parameters({})

        return ub

    def _update_calib_ubsed(self, a, b=None, remove_elem=False):
        """ see :func:`hydrac.instruments.aquascat.Aquascat._update_calib_aqa`
        """

        rr = glob.glob(constants.dir_ubsed_calib + '*.xml')
        rr.sort()
        

        if remove_elem is False:
            if rr==[]:
                uiui = ParamContainer(tag='CalibUB')
            else:
                uiui = ParamContainer(path_file=rr[-1])
            uiui._set_child(a, b)
            uiui._save_as_xml(path_file=constants.dir_ubsed_calib +
                              'calib_UBSED.xml', find_new_name=False,
                              overwrite=True)
        else:
            uiui = Parameters(ParamContainer(path_file=rr[-1]))
            try:
                print(a)
                del uiui[a]
               
            except(KeyError):
                pass
            yoyo = ParamContainer(tag='CalibUB')
            for a, b in uiui.items():
                yoyo._set_child(a, b)

            yoyo._save_as_xml(path_file=constants.dir_ubsed_calib +
                              'calib_UBSED.xml', find_new_name=False,
                              overwrite=True)

    
    def psiCal(self, f, at, r):
        """Compute the near field correction for each transducer channel."""
        PSI = np.zeros((len(r), len(f)))
        lamb = 1500 / f
        rn = np.pi * (at ** 2) / lamb
        for k in range(0, len(f)):
            for i in range(0, len(r)):
                if r[i] > 2 * rn[k]:
                    PSI[i, k] = 1
                else:
                    PSI[i, k] = (1/3) * (2+2 * rn[k] / r[i])
        return PSI

