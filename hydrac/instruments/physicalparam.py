#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multiparamter instruments (:mod:`hydrac.instruments.physicalparam`)
===================================================================

Multiparameter devices (ex. CTD) handled by the package.
Reads and stores data from text/csv raw files


.. autoclass:: PhysicalParam
   :members:
   :private-members:

"""


from .instruments import Instrument
from hydrac.util.parameters import Parameters
from hydrac.instruments.mod_deployment import ModeDeployment
# from hydrac.instruments.korexo import KorExo


class PhysicalParam(Instrument):
    """Base class : :class:`hydrac.instruments.instruments.Instrument`.

    This class is meant to be subclassed, not instantiated directly.
    It relates all non-acoustic based instruments to the same common
    Instrument class

    The PhysicalParam class handles all non-acoustic based device data. These
    data are point-wise data, contained in the modified dictionnary ``param``
    (:class:`hydrac.util.parameters.AttrDict`). PhysicalParam contains methods
    dedicated to their processing.

    .. image:: ../_image/ctd.pdf
       :width: 60%
       :align: center

    The param structure is common to all instruments. Each param
    can contain several sets of profiles (ex. ``param.P0``, ``param.P1``...),
    the structure of which is described in the table below.

    .. list-table:: Description of the param container
       :widths: 25 15 15 50
       :header-rows: 1

       * - Parameter Name
         - Type
         - Size
         - Description
       * - Depth, adepth
         - float
         - N (number of samples over time)
         - Depth (m), averaged depth (m)
       * - Temperature, aT
         - float
         - N
         - Temperature (°C), averaged Temperature (°C)
       * - Salinity, aS
         - float
         - N
         - Salinity, averaged salinity (PSU)
       * - Turbidity, aK
         - float
         - N
         - Turbidity, averaged turbidity (NTU)
       * - time, at
         - float
         - N
         - Time, averaged time (s)
       """

    def __init__(self):
        Instrument.__init__(self)
        self.instr_type = 'param_phy'
        # créer classe directement pour les param et pour les meta =>
        # y intégrer directment les fonctions de display
        # de sauvegarde, de chargement si déjà existants....
        self.param = Parameters({})
        self.meta = Parameters({})

    def preproc_phy_shape(self):
        """ Shapes the acoustic data within the common structure defined in the
        table up.
        The class :class:`hydrac.instruments.mod_deployment.ModDeployment` is
        called and prompt the user to shoose how the isntrument was deployed
        (Mooring, Cast). The user is then prompted to indicated whether and how
        he wishes to average the data (according to depth cells, time ...)"""

        if self.param.__len__():
            from hydrac.model.water import Water
            ModeDeployment(self)
            supl = ''
            if self.mode_depl == 'Mooring':
                supl = '_T'

            for i in range(0, len(self.param)):
                self.param['P' + str(i)].w_obj =\
                    Water(T=self.param['P' + str(i)]['aT' + supl],
                          S=self.param['P' + str(i)]['aSal' + supl],
                          P=self.param['P' + str(i)]['adepth' + supl])
                self.param['P' + str(i)
                           ].w_obj.paramw.update({'t': self.param['P' + str(i)
                                                                  ].at})

        else:
            print('There is no physical parameter data. Need to switch ' +
                  'to custom water param definition.')
            return
