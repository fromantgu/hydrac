#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Adcp instruments (:mod:`hydrac.instruments.adcp`)
=================================================

.. autoclass:: Ad2cp
   :members:
   :private-members:


"""
# from .instruments import instrument
from .acoustique import Acoustic
from hydrac.util import constants
import numpy as np
import glob
import os
import datetime
from tkinter import filedialog
from tkinter import Tk
from hydrac.util.parameters import Parameters
from hydrac.util.paramcontainer import ParamContainer
from hydrac.util.query import query_yes_no, query_number
import hydrac.util.dolfyn as df

# %gui inline
#    a=hydrac.Instruments.aquascat.aquascat('nnn','lll')
#    a.param=hydrac.aquascat.aquascat.batch_read(a.filepath,'',a.param)
#    test=a.param
#    test[a.filepath[0]]['AbsRxFrequency']


class Ad2cp(Acoustic):
    """ Ad2cp instruments class
    
    Base class : :mod:`hydrac.instruments.acoustique.Acoustic`
    
    The AD2CP is a single frequency Doppler velocity profiler. It measures the
    Doppler phase shifts induced by the moving targets (suspended particles) in
    the water column. As well as measuring the multi-component velocity, it is
    also capable of recording the backscattered intensity, which itself is of
    interest in hydrac.
    
    .. image:: ../_image/adcp.pdf
       :width: 100%
       :align: center
    
    The Ad2cp class reads the raw binary files from Nortek Signature
    instruments and stores the valuable information into the ``param`` 
    modified dictionnary with the common shape handled by hydrac
    (see :mod:`hydrac.instruments.acoustique`).
    
    The way the data are to be considered is similar to the example shown in
    :mod:`hydrac.instruments.aquascat`.
    
    Parameters
    ----------
    name : str, {'Campaign_1'}

      Tag name, for personal usage, for instance to add a campaign name."""

    def __init__(self, name):
        Acoustic.__init__(self)
        self.name = name
        self.instr_name = 'ad2cp'
        self.adcp_presets = Parameters(constants.ADCP)
        self.tag_M = 'mono'
        self.filepath, self.tempdir, self.tag_adcpType = self.file_select()
        self.batch_read()


    def _set_calib_ad2cp(self):
        """Calibration coefficients for each transducer are unique to each
        transducer. As such, each instrument comes with a different set of
        calibration coefficients Kt. Those are stored in xml file generated
        (if non-existant) or updated (with :func:`_update_calib_aqa`) with the
        instrument serial number and the Kts. This xml is accessed/generated
        through the class :class:`hydrac.util.paramcontainer.ParamContainer`.
        """
        rr = glob.glob(constants.dir_adcp_calib + '*.xml')
        rr.sort()
        try:
            adcp = Parameters(ParamContainer(path_file=rr[-1]))
        except(IndexError, AttributeError):
            adcp = Parameters({})
        return adcp

    def _update_calib_adcp(self, a, b=None, remove_elem=False):
        """Update the calibration xml file either by adding a new instrument
        or deleting the coefficients of a particular instrument. This xml is
        accessed/generated through the class
        :class:`hydrac.util.paramcontainer.ParamContainer`.

        Parameters
        ----------

        a : str

          Instrument serial number read from binary files and stored in
          ``param``

        b=None : dict

          Contains the calibration coefficients, prompted by the user if the
          instrument is used for the first time in hydrac.

        remove_elem=False : bool

          Removes the information relative to the serial number a in the xml.

        """
        rr = glob.glob(constants.dir_adcp_calib + '*.xml')
        rr.sort()
        if remove_elem is False:
            uiui = ParamContainer(path_file=rr[-1])
            uiui._set_child(a, b)
            uiui._save_as_xml(path_file=constants.dir_adcp_calib +
                              'adcp_calib.xml', find_new_name=False,
                              overwrite=True)

    def file_select(self):
        """ User input selection of the target files to read """
        root = Tk()
        filez = filedialog.askopenfilenames(parent=root, title='Choose a file')
        root.withdraw()  # use to hide tkinter window
        filepath = list(filez)
        filepath.sort()
        filepath = tuple(filepath)
        tempdir = os.path.dirname(os.path.abspath(filez[0]))
        tag_adcpType = [os.path.split(os.path.abspath(filez[i]))[1][-5]
                        for i in range(0, len(filez))]
        return filepath, tempdir, tag_adcpType

    def param_shape_AD2CP(self, inp):
        """Reshapes the modified dictionnary ``param`` to fit the standard key
        names and data shape used throughout hydrac"""

        inp2 = Parameters({'ad2cp': {}})
        inp3 = Parameters({})
        
        time_raw=np.array(inp.coords['time_avg'])
        Yall=[]
        date_format = "%Y/%m/%d %H:%M:%S.%f"
        for iop in range(len(time_raw)):
            tp=str(time_raw[iop]).partition('T')
            subtime=tp[0].replace('-','/') + ' ' + tp[2][:-3]
            
            Yall.append(datetime.datetime.strptime(subtime,date_format))
            
        inp2.BurstTime = datetime.datetime.timestamp(Yall[0])
        inp2.PingRate = inp.attrs['fs']
        
        inp2.NumChannels = len(inp.amp_avg)
        
        # pUS=Parameters({})
        # [pUS.update({'pUS'+str(i+1):inp.param_us_dicts[str(i+1)][list(inp.param_us_dicts[str(i+1)].items())[0][0]]}) for i in range(inp2.NumChannels)]
        # dUS=Parameters({})
        # [dUS.update({'dUS'+str(i+1):inp.data_us_dicts[str(i+1)][list(inp.data_us_dicts[str(i+1)].items())[0][0]]}) for i in range(inp2.NumChannels)]

        # dec=[]
        
        # [dec.append(len(dUS['dUS' +str(i+1)]['echo_avg_profile']['time'])) for i in range(inp2.NumChannels)]

        # dec_it=np.min(dec)
        # dec_id=np.where(dec==dec_it)[0][0]
        inp2.NumProfilesSamples = np.ones((inp2.NumChannels)) *\
            len(Yall)
        inp2.NumAverage = np.ones((inp2.NumChannels))
        inp2.SampleRate = inp.attrs['fs']
        
            
        inp2.BinLengthMM =  np.squeeze(np.ones((1,inp2.NumChannels)))*inp.cell_size_avg*1000
        inp2.StartBin = np.squeeze(np.zeros((1,inp2.NumChannels)))+float(inp.range_avg[0])
        inp2.NumBins = np.squeeze(np.zeros((1,inp2.NumChannels)))+len(inp.range_avg)

       
        NumBins=list(map(int,inp2.NumBins))
        inp2.Frequency=np.tile(int(inp.inst_model.replace('Signature',''))*1000,inp2.NumChannels)
        inp2.FrequencyRx=np.tile(int(inp.inst_model.replace('Signature',''))*1000,inp2.NumChannels)
        inp2.RawIntensity = np.transpose(np.array(inp.amp_avg),(1,2,0))
        inp2.BinRange = np.tile(np.array(inp.range_avg),(inp2.NumChannels, 1)).T
        inp3.echo_sat=np.transpose(np.array(inp.corr_avg),(1,2,0))
        inp3.vel_avg=np.transpose(np.array(inp.vel_avg),(1,2,0))
        inp3.vel_mag=np.array(inp.mag_avg).T
        inp3.status=np.array(inp.status_avg)
        inp3.c=np.array(inp.c_sound_avg)
        inp3.temp=np.array(inp.temp_avg)
        inp3.heading=np.array(inp.heading_avg)
        inp3.pitch=np.array(inp.pitch_avg)
        inp3.roll=np.array(inp.roll_avg)
        inp3.roll=np.array(inp.roll_avg)
        inp2.battery=np.array(inp.batt_avg)
        inp3.accel=np.array(inp.accel_avg).T
        inp2.TxGain=np.array(inp.xmit_energy_avg)
        inp2.RxGain =  np.zeros(np.shape(inp2.TxGain))


       
        inp2.NumPingsTot = inp2.NumAverage[0]*inp2.NumProfilesSamples[0]

        inp2.TransducerName = np.tile(np.array(str(inp2.Frequency) + ' MHz',
                                               dtype=str),
                                      (inp2.NumChannels))
        inp2.PulseLength = inp2.BinLengthMM * 2 / np.mean(inp3.c)/1000
        
        inp2.time = []
        [inp2.time.append(datetime.datetime.timestamp(i)) for i in Yall]
        inp2.time = np.array(inp2.time)
        inp2.TVG = np.tile(np.zeros((np.size(inp2.BinRange, 0), 1)),
                           (1, inp2.NumChannels))
                
        inp2.MeanProfile = np.nanmean(inp2.RawIntensity, 1)
        inp2.SerialNum = str(inp.SerialNum_avg)
        inp2.pressure = np.array(inp.pressure_avg)
        inp2.depth_raw = np.zeros(np.shape(inp2.pressure))
        inp2.depth = inp2.depth_raw
        inp2.sal = np.zeros((int(inp2.NumProfilesSamples[0]))) * np.nan
        inp2.NumSamples = inp2.NumProfilesSamples
        inp2.FileName = os.path.split(self.currfilepath)[-1]
        inp3.time_aux=inp2.time
        inp2.ad2cp = inp3
       
        inp2.calib = self._set_calib_ad2cp()
        
        try:
            TransducerKt = inp2.calib['SN' + str(inp2.SerialNum)].Kt
            inp2.TransducerKt = []
            [inp2.TransducerKt.append([a, b]) for a, b in TransducerKt
              if a in inp2.FrequencyRx]
            print("calib ok")            
            
                
        except(AttributeError):
            pass
        ex = False
        if 'SN' + str(inp2.SerialNum) not in inp2.calib.keys():
            inp2.calib.update({'SN' + str(inp2.SerialNum): Parameters({})})
            ex = True
            if query_yes_no('No Kt values for AQA SN' + str(
                    inp2.SerialNum) + ' - Do you want to enter Kt manually ? '):
        
                Kt = list()
                for ui in range(len(inp2.FrequencyRx)):
                    Kt.append((inp2.FrequencyRx[ui],
                                query_number('Kt channel' + str(ui) + ' F = ' +
                                            str(inp2.FrequencyRx[ui]))))
                inp2.calib['SN' + str(inp2.SerialNum)].update({'Kt': Kt})
        
            else:
                Kt = []
                for ui in range(len(inp2.FrequencyRx)):
                    Kt.append((inp2.FrequencyRx[ui], 1))
                inp2.calib['SN' + str(inp2.SerialNum)].update({'Kt': Kt})
        
            
            TransducerKt = inp2.calib['SN' + str(inp2.SerialNum)].Kt
            inp2.TransducerKt = []
            [inp2.TransducerKt.append([a, b]) for a, b in TransducerKt
              if a in inp2.FrequencyRx]
            
            
            
        if ex:
            if query_yes_no('Do you wish to save current' +
                            ' instrument calibration parameters ?'):
                self._update_calib_ubsed('SN' + str(inp2.SerialNum),
                                        inp2.calib['SN' +
                                                  str(inp2.SerialNum)])
            else:
                print('je pas au bon endroit')    
        

        return inp2  

    def batch_read(self):
        """Read a list of udt raw UB_MES file using the method :func:`read_UB`
        """

        for kk in range(len(self.filepath)):
            self.currfilepath = self.filepath[kk]
            
            tmp = self.param_shape_AD2CP(df.read(self.filepath[kk]))
            self.param.update({"P"+str(kk): tmp})

            del self.currfilepath

class WrongFileTypeError(Exception):
    pass



