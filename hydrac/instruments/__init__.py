"""
Instruments
===========

This subpackage ensures the interfacing between the collected data (acoustic and non acoustic) and the actual inversion procedures. It ensures that all files are structured the same way no matter the chosen instruments so that the inversion routine can communicate with them. It also modifies the structure according the deployment settings of the instruments, such as mooring, surface station…


.. autosummary::
   :toctree:

   instruments
   acoustique
   physicalparam
   adcp
   aquascat
   ek60
   ub_mes
   korexo
   sbe
   mod_deployment

"""
#"""
#Instruments
#===========
#
#.. _instruments:
#.. currentmodule:: hydrac.instruments
#
#This subpackage ensures the interfacing between the collected data (acoustic and non acoustic) and the actual inversion procedures. It ensures that all files are structured the same way no matter the chosen instruments so that the inversion routine can communicate with them. It also modifies the structure according the deployment settings of the instruments, such as mooring, surface station…
#
#
#.. autosummary::
#   :toctree:
#
#   instruments
#   adcp
#   aquascat
#   ek60
#
#"""

#Si le fichier __init__.py du paquet définit une liste nommée __all__, cette liste sera utilisée comme liste des noms de modules devant être importés lorsque from package import * est utilisé. C’est le rôle de l’auteur du paquet de maintenir cette liste à jour lorsque de nouvelles version du paquet sont publiées.

from ..instruments import acoustique
from ..instruments import ublab3c
from ..instruments import ad2cp
from ..instruments import ubsediflow
from ..instruments import aquascat
from ..instruments import peacock
from ..instruments import ub_mes
from ..instruments import ek60
from ..instruments import adcp
from ..instruments import korexo
from ..instruments import sbe
from ..instruments import mod_deployment
#from .acoustique import acoustic
#from .aquascat import *

__all__ = ['instruments', 'acoustique', 'aquascat',
           'adcp', 'auxiliary','mod_deployment']
