
"""
Resampling Class (:mod:`hydrac.calcul.resample`)
================================================

.. autoclass:: Resample
   :members:
   :private-members:

"""

import numpy as np
import collections

class Resample(object):
    """Blablabla"""

    def __init__(self, obj1, binsize, target=None, vert_on=-1):

        self.obj1 = obj1
        self.binsize = binsize
        self.vert_on=vert_on
        self.bad_flag=[]
        

        if self.obj1.mode_depl == 'Mooring':

            if obj1.instr_type == 'param_phy':
                if target is None:
                    target=dict({'Depth':'adepth_T','time':'at','Temperature':'aT_T','Salinity':'aSal_T','Turbidity':'aK_T'})
                self.obj1.tmp = self._model_key_for_time('Depth',target=target.keys())
                
    
                [[self.obj1.param['P' + str(
                        self.obj1.tmp[ki][i][2])].update({target[self.obj1.tmp[ki][i][1]]:np.atleast_2d(self.obj1.tmp[ki][i]
                                                          [0]).T})
                  for i in range(0, len(self.obj1.tmp[ki]))]
                    for ki in range(0, len(self.obj1.param))]
                print('les params sont lew uviants {}'.format(self.obj1.param.P0.keys()))
    
                del self.obj1.tmp
            
            if obj1.instr_type == 'acoustic':
                
                self.obj1.tmp = self._model_key_for_time('Intensity')
                
                [[self.obj1.param['P' + str(
                        self.obj1.tmp[ki][i][2])].update({self.obj1.tmp[ki][i][1] +
                                                          '_T':self.obj1.tmp[ki][i]
                                                          [0]})
                  for i in range(0, len(self.obj1.tmp[ki]))]
                    for ki in range(0, len(self.obj1.param))]
    
                del self.obj1.tmp
                print('I aml here hoho : {}'.format(self.vert_on))
                if self.vert_on > 0:
                    self.binsize = self.vert_on
                    for i in range(0, len(self.obj1.param)):
                        print(i)
    
                        tmp = self.\
                            resample_depth(self.obj1.param['P' + str(i)]['BinRange'][:,0],
                                           self.obj1.param['P' + str(i)]['Intensity_T'])
                        print('I aml here hoho')
                        self.obj1.param['P' + str(i)].adepth = np.array(tmp[0])
                        self.obj1.param['P' + str(i)].Intensity_T = np.array(tmp[-1])


        elif self.obj1.mode_depl == 'Cast':
            for i in range(0, len(self.obj1.param)):
                print(i)
#                    self.resample_depth(self.obj1['P'+ str(i)])
                if obj1.instr_type == 'param_phy':

                    print(i)
                    tmp = self.\
                        resample_depth(obj1.param['P' + str(i)]['Depth'],
                                       obj1.param['P' + str(i)]['time'],
                                       obj1.param['P' + str(i)]['Temperature'],
                                       obj1.param['P' + str(i)]['Salinity'],
                                       obj1.param['P' + str(i)]['Turbidity'
                                                                ])

                    self.obj1.param['P' + str(i)].adepth = np.array(tmp[0])
                    self.obj1.param['P' + str(i)].at = np.array(tmp[1])
                    self.obj1.param['P' + str(i)].aT = np.array(tmp[2])
                    self.obj1.param['P' + str(i)].aSal = np.array(tmp[3])
                    self.obj1.param['P' + str(i)].aK = np.array(tmp[4])

                elif obj1.instr_type == 'acoustic':
                    
                    c_a, c_b = self.depth_cal(obj1.param['P' + str(i)])
                    
                    tmp = self.\
                        resample_depth(obj1.param['P' + str(i)]['depth'] *
                                       c_a + c_b,
                                       obj1.param['P' + str(i)]['Intensity'])
                    self.obj1.param['P' + str(i)].adepth = np.array(tmp[0])
                    self.obj1.param['P' + str(i)].asv = np.array(tmp[-1])

                    try:
                        for j in range(0, self.obj1.param['P' + str(i)
                                                          ].NumChannels):
                            self.obj1.param['P' + str(i)]['asv' + str(j+1)] = \
                                np.array(tmp[j+1])
                    except(IndexError):
                        pass

    def _model_key_for_time(self,key,target=None):
        
        if target is None or isinstance(target,collections.KeysView)==False:
            target=self.obj1.param.P0.keys()
        
        if len(np.shape(self.obj1.param.P0[key]))>1:
            incr=0
        else:
            incr=1

        u=[[self.resample_time(
                    self.obj1.param['P' + str(i)][k],
                    np.where(np.array(np.shape(
                            self.obj1.param['P' + str(i)][k])
                                      ) == np.shape(self.obj1.param[
                                          'P' + str(i)][key])[1-incr]
                             ), np.size(self.obj1.param['P' + str(i)
                                                        ][key], 1-incr), k, i)
                for k in target
                if np.size(np.where(np.array(np.shape(
                        self.obj1.param['P' + str(i)][k])) ==
                            np.shape(self.obj1.param['P' + str(i)
                                                     ][key])[1-incr])) == 1]
                for i in range(0, len(self.obj1.param))]
    
    
        return u
        
    def depth_cal(self,x):
        
        if self.obj1.instr_name=='aquascat':
            c_a = x.calib['SN' + str(x.SerialNum[0]) +
                               str(x.SerialNum[1])].cal_depth1.a
            c_b = x.calib['SN' + str(x.SerialNum[0]) +
                       str(x.SerialNum[1])].cal_depth1.b
        else:
            c_a=1
            c_b=0
        return c_a, c_b
            
        
    def clean_argsout(self,argsout):
        koko = np.zeros(np.shape(argsout))
        for ii in range(0, np.size(koko, 0)):
            for jj in range(0, np.size(koko, 1)):
                koko[ii, jj] = argsout[ii, jj]
        return koko
    
    def _reduce(self,a):
        d=a[0]
        s=a[-1]
        print("Fifth OK d: {}".format(np.shape(d)))
        print("Sixth OK s: {}".format(np.shape(s)))

        pointer=np.min(np.where(d==np.max(d))[0])
        pointer_m=np.min(np.where(d[0:pointer]==np.min(d[0:pointer]))[0])
        

        u1=np.max(np.where(d[0:pointer]<d[pointer_m]+0.01*(d[pointer]-d[pointer_m]))[0])

        u2=np.min(np.where(d[pointer:]<d[pointer_m]+0.01*(d[pointer]-d[pointer_m]))[0])+pointer-1
        safety_window=0.1*(u2-u1)/len(d)
        u1=np.max([int(u1-np.floor(safety_window*len(d))),0])
        u2=np.min([int(u2+np.floor(safety_window*len(d))),len(d)])
        
        b=[]
        b.append(d[u1:u2])
        b.append(s[:,u1:u2,:])
        
        return b

    def resample_time(self, dd, t, dim, k, i):

        binsize = np.round(self.binsize /
                           (self.obj1.param['P' + str(i)].time[1] -
                            self.obj1.param['P' + str(i)].time[0]))
        if self.binsize == 0:
            print('No averaging requested')
            return dd, k, i
            
        if binsize <= 1:
            print('no averaging possible at this time interval. Please ' +
                  'consider increasing your averaging window at a binsize ' +
                  'greater than ' +
                  '{}s'.format(2 *
                               (self.obj1.param['P' + str(i)].time[1] -
                                self.obj1.param['P' + str(i)].time[0])))

            sub_Pset = np.zeros(np.shape(dd))
            return sub_Pset, k, i

        ind_ = np.shape(dd)
        ty = list(zip(np.shape(dd), np.arange(0, len(np.shape(dd)))))
        [ty.remove(x) for x in ty if x[1] == t[0]]
        h = [y for x, y in ty]
        h = list(t[0]) + h
        dd = np.transpose(dd, tuple(h))
        maxbin = np.ceil(dim / binsize)
        sub_Pset = np.zeros(np.shape(dd))
        sub_Pset = sub_Pset[0::int(binsize), ]
        kl = 0
        for u in range(0, np.size(dd, 0), int(binsize)):
            sub_Pset[kl] = np.nanmean(dd[u: u+5, ], 0)
            kl += 1

        sub_Pset = np.transpose(sub_Pset, tuple(h))

        return sub_Pset, k, i

    def resample_depth(self, *args):
        args0 = list(args)
        args = list(args)
        
        if len(args) <= 2:
            _dim1 = np.size(args[1], 2)
            if self.obj1.mode_depl=='Cast':
                _dim2 = np.size(args[1], 0)
                print("First OK")
            else:
                _dim2 = np.size(args[1], 1)
        else:
            _dim1 = len(args) - 1
            _dim2 = 1
            print("Second OK")
        if len(np.shape(list(args[1]))) > 2:
            if self.obj1.mode_depl=='Cast':
                test2 = np.transpose(args[1], (1, 0, 2))
            else:
                test2 = args[1]
            args[1] = test2.reshape((test2.shape[0], -1), order='F')
            args[0] = np.atleast_2d(args[0]).T
            print("Third OK")


        if self.obj1.instr_name == 'aquascat':
            pass
        else:
            args[0] = args[0] - min(args[0])
        A = np.column_stack(args)
        binsize = self.binsize
        print("Fourth OK : {}".format(binsize))


        # Find all empty values (NaN or Inf) in the profile data
        A[np.where(A[:, 0] <= 0)] = np.NaN
        A = np.delete(A, np.where(np.isnan(A[:, 0])), axis=0)

        if np.shape(A)[0] == 0:
            print('Data might be corrupted...')
            argsout = list(args)
            self.bad_flag.append(1)
            return argsout
        self.bad_flag.append(0)

        if self.binsize == 0:
            print('No averaging requested')
#            return list(args)
            if self.obj1.instr_type == 'param_phy':
                return args0
            else:
                return self._reduce(args0)
        # Set up bins
        # NB bins from beginning to the end of experience
        print('Setting up bins')

        #calculation of the bin number
        nbbin = np.ceil( max(A[:, 0]) / binsize )
        adepth = np.arange(1, nbbin + 1) * binsize - binsize / 2
        aone = np.spacing(1) * np.ones((int(nbbin), 1))
        ndz = np.zeros((int(nbbin), 1))     

        nd = np.zeros((int(nbbin), 1))

        for i in range(1, _dim1 + 1):
            exec('a' + str(i) + '=np.spacing(1) * ' +
                 'np.ones((int(nbbin),_dim2))')

        # loop over data set, sorting heartbeat per 
        # minute values into time bins
        print('Binning data now: ')
        ctr = 0
        A = A[A[:, 0].argsort(), ]


        for k in range(0, len(A[:, 0])):
            
            t = A[k, 0]
            
            l = int(np.ceil(t / binsize) - 1)
            ndz[l] = ndz[l] + 1

            ctr = ctr + 1

            for i in range(1, _dim1 + 1):
                exec('z' + str(i) + '=A[k, ' + str(_dim2 * (i - 1) + 1) + ':' +
                     str(_dim2 * (i) + 1) + ']')

                exec('a' + str(i) + '[' + str(l) + ',:] = a' + str(i) +
                     '[' + str(l) + ',:] + z' + str(i))
            nd[l] = nd[l] + 1

        A = np.delete(A, np.where(np.isnan(A[:, 0])), axis=0)

        argsout = []
        argsout.append(adepth)
        for i in range(1, _dim1 + 1):

            exec('a' + str(i) + ' = np.multiply(a' + str(i) +
                 ',1/np.tile(ndz,np.size(a' + str(i) + ',1)))')
            exec('a' + str(i) + '[np.where(np.isinf(a' + str(i) + '))]=np.NaN')
            exec('argsout.append(a' + str(i) + ')')

        if _dim2 != 1:
            RRtmp = np.column_stack(argsout[1:])
            if self.obj1.mode_depl=='Cast':
                argsout.append(np.transpose(RRtmp.reshape(-1, _dim1, _dim2),
                                        (2, 0, 1)))
            else:
                argsout.append(np.transpose(RRtmp.reshape(-1, _dim1, _dim2),
                                        (0, 2, 1)))

        return argsout

