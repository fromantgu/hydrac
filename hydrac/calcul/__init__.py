"""Utilities for treatments
===========================

This subpackage contains specialized functions and classes for efficient
calculations - employs seawater package from CSIRO.

.. autosummary::
   :toctree:

   seawater
   resample

"""

from ..calcul import resample
from ..calcul import seawater
