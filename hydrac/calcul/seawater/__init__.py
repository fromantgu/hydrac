"""Seawater library
====================================================

Library of methods usually used for water parameters calculations such as water density, water absorption or sound velocity in water...

Users are particularly concerned with the following already defined topologies:



"""


from __future__ import division, absolute_import

__version__ = '3.3.4'

from .geostrophic import bfrq, svan, gpan, gvel
from .extras import dist, f, satAr, satN2, satO2, swvel
from .library import cndr, salds, salrp, salrt, seck, sals, smow
from .eos80 import (adtg, alpha, aonb, beta, dpth, g, salt, fp, svel,
                    pres, dens0, dens, pden, cp, ptmp, temp)
