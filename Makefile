NAME=hydrac

# Second tag after tip is usually the latest release
RELEASE=$(shell hg tags -T "{node|short}\n" | sed -n 2p)

develop:
	pip install -e .[dev]

develop_user:
	pip install -e .[dev] --user


clean_pyc:
	find $(NAME) -name "*.pyc" -delete
	find $(NAME) -name "__pycache__" -type d | xargs rm -rf

cleanpythran:
	find $(NAME) -type d -name __pythran__ | xargs rm -rf

clean:
	rm -rf build dist *.egg-info __pycache__

cleanall: clean cleanpythran

shortlog:
	@hg log -M -r$(RELEASE): --template '- {desc|firstline} (:rev:`{node|short}`)\n'

black:
	black -l 82 $(NAME)

tests:
	$(NAME)-test -v

tests_mpi:
	mpirun -np 2 $(NAME)-test -v

_tests_coverage:
	mkdir -p .coverage
	coverage run -p -m $(NAME).util.testing -v
	TRANSONIC_NO_REPLACE=1 coverage run -p -m $(NAME).util.testing -v
	TRANSONIC_NO_REPLACE=1 mpirun -np 2 coverage run -p -m $(NAME).util.testing -v

_report_coverage:
	coverage combine
	coverage report
	coverage html
	coverage xml
	@echo "Code coverage analysis complete. View detailed report:"
	@echo "file://${PWD}/.coverage/index.html"

coverage: _tests_coverage _report_coverage

coverage_short:
	mkdir -p .coverage
	TRANSONIC_NO_REPLACE=1 coverage run -p -m $(NAME).util.testing -v
	make _report_coverage

lint:
	pylint -rn --rcfile=pylintrc --jobs=$(shell nproc) $(NAME) --exit-zero

install:
	python setup.py install
