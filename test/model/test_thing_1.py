"""
TEST
"""

import unittest
import sys
from unittest.mock import patch
from hydrac.model.particle import Particle

class TestExistingParticle(unittest.TestCase):
    
    
    def setUp(self):
        self.P1=Particle('TFS')

    def test_param_init_1(self):
        thing_id = {'c': 1600.0, 'cp': 1600, 'cs': 0, 'mod_num': 2, 'rhos': 1200}
        thing1 = self.P1.param
        thing_id2 = 'ORG'
        thing2 = self.P1.nature
        thing_id3 = 'THQ'
        thing3 = self.P1.mtype
        thing_id4 = 'SPH'
        thing4 = self.P1.shape
        self.assertEqual(thing_id, thing1)
        self.assertEqual(thing_id2, thing2)
        self.assertEqual(thing_id3, thing3)
        self.assertEqual(thing_id4, thing4)

    
class TestNonExistingParticle(unittest.TestCase):
    
#    @patch('test.model.test_thing_1.get_input', return_value='y')
    @patch('builtins.input', lambda: 'y')
    def setUp(self):
        self.P1=Particle('test',rhos=2650, c=3730.504880933232, cp=5500,
                 cs=3500,mtype='EMP',nature='MIN',shape='SPH',mod_num=0)

    def test_particle_save(self):
        P=Particle('test')
        self.assertEqual(self.P1.param,P.param)

    def tearDown(self):
        self.P1._update_model_presets('test',remove_elem=True)


#from unittest.mock import patch
#from unittest import TestCase
#
####
#
#def get_input(text):
#    return input(text)
#
#
#def answer():
#    ans = get_input('enter yes or no')
#    if ans == 'yes':
#        return 'you entered yes'
#    if ans == 'no':
#        return 'you entered no'
#
#
#class Test(TestCase):
#
#    # get_input will return 'yes' during this test
#    @patch('yourmodule.get_input', return_value='yes')
#    def test_answer_yes(self, input):
#        self.assertEqual(answer(), 'you entered yes')
#
#    @patch('yourmodule.get_input', return_value='no')
#    def test_answer_no(self, input):
#        self.assertEqual(answer(), 'you entered no')
####
#
#import unittest
#
#class BaseTestCases:
#
#    class BaseTest(unittest.TestCase):
#
#        def testCommon(self):
#            print 'Calling BaseTest:testCommon'
#            value = 5
#            self.assertEquals(value, 5)
#
#
#class SubTest1(BaseTestCases.BaseTest):
#
#    def testSub1(self):
#        print 'Calling SubTest1:testSub1'
#        sub = 3
#        self.assertEquals(sub, 3)
#
#
#class SubTest2(BaseTestCases.BaseTest):
#
#    def testSub2(self):
#        print 'Calling SubTest2:testSub2'
#        sub = 4
#        self.assertEquals(sub, 4)
#
#if __name__ == '__main__':
#    unittest.main()
####
#
#class BaseTest(unittest.TestCase):
#
#    @classmethod
#    def setUpClass(cls):
#        if cls is BaseTest:
#            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
#        super(BaseTest, cls).setUpClass()
####
#
#class BaseTest(unittest.TestCase):
#    @classmethod
#    def setUpClass(cls):
#        cls._test_methods = []
#        if cls is BaseTest:
#            for name in dir(cls):
#                if name.startswith('test') and callable(getattr(cls, name)):
#                    cls._test_methods.append((name, getattr(cls, name)))
#                    setattr(cls, name, lambda self: None)
#
#    @classmethod
#    def tearDownClass(cls):
#        if cls is BaseTest:
#            for name, method in cls._test_methods:
#                setattr(cls, name, method)
#            cls._test_methods = []
####
#
#class BaseTest(unittest.TestCase):
#    def setUp(self):
#        print 'Calling BaseTest:testCommon'
#        value = 5
#        self.assertEquals(value, 5)
#
#
#class SubTest1(BaseTest):
#    def testSub1(self):
#        print 'Calling SubTest1:testSub1'
#        sub = 3
#        self.assertEquals(sub, 3)
#
#
#class SubTest2(BaseTest):
#    def testSub2(self):
#        print 'Calling SubTest2:testSub2'
#        sub = 4
#        self.assertEquals(sub, 4)
#
####