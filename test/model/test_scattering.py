#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 17:29:39 2018

@author: fromant9gu
"""


import unittest

import sys
import unittest.mock as mock
from unittest.mock import patch,Mock,MagicMock
import hydrac
from hydrac.model.particle import Particle
import numpy as np
from hydrac.util.parameters import Parameters
from hydrac.model.water import Water
from hydrac.model.scattering.scattering import Scattering, Emp, Thq

def template_output(ind=None):
    if ind is None:
            
        return[(np.arange(0,100),np.arange(0,100)),
                (np.arange(0,1000),np.arange(0,1000)),
                (np.arange(0,10000),np.arange(0,10000)),
                (np.arange(0,10000),np.arange(0,10000))]
    else:
        return[(np.arange(0,100).T,np.arange(0,100),np.arange(0,100)),
                (np.arange(0,1000).T,np.arange(0,1000),np.arange(0,1000))]
    
class TestScattering(unittest.TestCase):
    
#    m1=Mock(spec=Particle)
#    m2=Mock(spec=Water)
#    m3=Mock(spec=Scattering)

#    @patch('hydrac.model.scattering.scattering.Scattering')
#    @patch('hydrac.model.water.Water')
#    @patch('hydrac.model.particle.Particle')
#    def setUp(self,m1,m2,m3):
#        mock_w=m2(c=1480,rho=1000)
#        mock_p=m1('Thorne2008')
##        self.S=Scattering(m1,mock_w,freq=1e6,tagM='multi',mode_depl='Cast') 
#        self.S1=m3(mock_p,mock_w,freq=1e6,tagM='multi',mode_depl='Cast')
#        self.S1.RES_model.assert_called_with()
#    
#    @patch('hydrac.model.scattering.scattering.Scattering')
#    def setUp(self,m1):
#    @mock.patch('hydrac.model.scattering.scattering.RES_model')
#    def setUp(self,res_mod):
#        w=Water(c=1480,rho=1000)
#        p=Particle('Thorne2008')
##        self.S=Scattering(m1,mock_w,freq=1e6,tagM='multi',mode_depl='Cast') 
#        self.S1=Scattering(p,w,freq=[1e6],tag_M='multi',mode_depl='Cast')
#        self.S2=Scattering(p,w,freq=[1e6],tag_M='multi',mode_depl='Cast',dsigma=0.5)
#    
#    def side_emp_thq():
##        mock = MagicMock(side_effect=[(np.atleast_2d(np.arange(0,100)),np.atleast_2d(np.arange(0,100))),(np.atleast_2d(np.arange(0,1000)),np.atleast_2d(np.arange(0,1000)))])
#        mock1 = MagicMock(side_effect=[(np.atleast_2d(np.arange(0,100)),np.atleast_2d(np.arange(0,100)))])
#        mock2 = MagicMock(side_effect=[(np.atleast_2d(np.arange(0,1000)),np.atleast_2d(np.arange(0,1000)))])
##        mock = MagicMock(side_effect=[np.atleast_2d(np.arange(0,120)),np.atleast_2d(np.arange(0,1200))])
##        return mock(),mock()
#        return mock1(),mock2()
#    def side_emp_thq():
#        
#        side_emp_thq=[]
#        return [side_emp_thq.append((np.arange(0,x),np.arange(0,x))) for x in [100,1000,10000,10000]]
    
    
    @patch('hydrac.model.scattering.scattering.Scattering.tmp_del')
    @patch('hydrac.model.scattering.emp.Emp.emp_min_func',side_effect=template_output())
    @patch('hydrac.model.scattering.thq.Thq.thq_fluid_func',side_effect=template_output())
    @patch('hydrac.model.scattering.thq.Thq.thq_elast_func',side_effect=template_output())
    @patch('hydrac.model.scattering.scattering.Scattering.ensemble_average_model',side_effect=template_output(1))
    #    @patch('hydrac.model.scattering.scattering.Scattering.calc_f')
#    def setUp(self,c,th,em,tmp_d):
    def setUp(self,ens,el,th,em,tmp_d):
        self.w=Water(c=1480,rho=1000)
        self.p1=Particle('Thorne2008')
        self.p2=Particle('TFS')
        self.p3=Particle('PMMA')
#        th.return_value=[np.arange(0,100),np.arange(0,100)]
#        self.S=Scattering(m1,mock_w,freq=1e6,tagM='multi',mode_depl='Cast') 
        self.S1=Scattering(self.p1,self.w,freq=[1e6],tag_M='mono',mode_depl='Cast')
        ens.assert_not_called()
#        print('aa = {}, bb = {}, c={}'.format(em.call_count,th.call_count,el.call_count))

        self.S2=Scattering(self.p2,self.w,freq=[1e6],tag_M='mono',mode_depl='Cast',dsigma=0.5)
#        print('aa = {}, bb = {}, c={}'.format(em.call_count,th.call_count,el.call_count))

        self.S3=Scattering(self.p3,self.w,freq=[1e6],tag_M='mono',mode_depl='Cast')
#        print('aa = {}, bb = {}, c={}'.format(em.call_count,th.call_count,el.call_count))

        self.tmp_d=tmp_d
        self.th=th
        self.em=em
        self.el=el
        self.ens=ens
        
#        c.assert_called_with(th,self.S2.scattering.ka)

    @patch('hydrac.model.scattering.scattering.Scattering.RES_model')
    @patch('hydrac.model.scattering.scattering.Scattering.ensemble_average_model')
    def test_func_called(self,ens_avg,res_mod):
#    def test_func_called(self,res_mod):
        
#        print('aa = {}, bb = {}, c={}'.format(self.em.call_count,self.th.call_count,self.el.call_count))
        
        
        assert self.em.call_count == 2
        assert self.th.call_count == 2 # mockek ensemnble_averaged during setUp
        assert self.el.call_count == 2
        res_mod.assert_not_called()
        self.S1.RES_model(am=3e-6, aM=300e-6, spacing='log', UND=None)
        ens_avg.assert_not_called()
        self.tmp_d.assert_called_with()
        self.S2.ensemble_average_model(1, dsigma=self.S2.scattering.dsigma, freq=self.S2.scattering.freq, na=1e4,am=3e-6, aM=300e-6, space='lin')
        self.tmp_d.assert_called_with()
        self.em.assert_called()
        self.th.assert_called()
        self.ens.assert_called()
        res_mod.assert_called_once_with(am=3e-6, aM=300e-6, spacing='log', UND=None)
        ens_avg.assert_called_with(1, dsigma=self.S2.scattering.dsigma, freq=self.S2.scattering.freq, na=1e4,am=3e-6, aM=300e-6, space='lin')
        self.assertEqual(self.S1.scattering.freq,[1e6])

#        self.S1.RES_model.assert_called_with()
#        self.S1.calc_f.assert_called_with()
#        self.S1.ensemble_average_model.assert_not_called()
#        self.S2.ensemble_average_model.assert_called_once()
    @patch('hydrac.model.scattering.emp.Emp.emp_min_func',return_value=template_output()[0])
    @patch('hydrac.model.scattering.thq.Thq.thq_fluid_func',return_value=template_output()[0])
    def test_calc_f(self,th2,em2):
        aa,gg=self.S1.calc_f(em2,np.atleast_2d(np.arange(0,100)))
        bb,cc=self.S2.calc_f(th2,np.atleast_2d(np.arange(0,100)))
        th2.assert_called_once()
        em2.assert_called_once()
    
    def test_scattering_thq(self):
        Sthq_simple_mono=Scattering(self.p2,self.w,freq=[1e6],tag_M='mono',mode_depl='Cast')
        Sthq_simple_multi=Scattering(self.p2,self.w,freq=[1e6,2e6],tag_M='multi',mode_depl='Cast')
        Sthq_simple_mono.scattering.c_0=Sthq_simple_mono.ensemble_average_model(Sthq_simple_mono.scattering.ka, dsigma=0.5, na=10)[0]
        Sthq_simple_multi.scattering.c_0=Sthq_simple_multi.ensemble_average_model(Sthq_simple_multi.scattering.ka, dsigma=0.5, na=10)[0]

        self.assertEqual(np.unique(Sthq_simple_mono.scattering.c_inf-Sthq_simple_multi.scattering.c_inf[0,:]),np.array([0]))
        self.assertEqual(np.unique(Sthq_simple_mono.scattering.c_infHR-Sthq_simple_multi.scattering.c_infHR[0,:]),np.array([0]))
        [self.assertAlmostEqual(a[0],b[0]) if a-b!=np.array([0.]) else self.assertEqual(a[0],b[0]) for a,b in zip(Sthq_simple_mono.scattering.c_0.T,np.atleast_2d(Sthq_simple_multi.scattering.c_0[0,:]).T) if a-b!=np.array([0.])]

    def test_scattering_emp(self):
        Semp_simple_mono=Scattering(self.p1,self.w,freq=[1e6],tag_M='mono',mode_depl='Cast')
        Semp_simple_multi=Scattering(self.p1,self.w,freq=[1e6,2e6],tag_M='multi',mode_depl='Cast')
        Semp_simple_mono.scattering.c_0=Semp_simple_mono.ensemble_average_model(Semp_simple_mono.scattering.ka, dsigma=0.5, na=10)[2]
        Semp_simple_multi.scattering.c_0=Semp_simple_multi.ensemble_average_model(Semp_simple_multi.scattering.ka, dsigma=0.5, na=10)[2]
        
        self.assertEqual(np.unique(Semp_simple_mono.scattering.c_inf-Semp_simple_multi.scattering.c_inf[0,:]),np.array([0]))
        self.assertEqual(np.unique(Semp_simple_mono.scattering.c_infHR-Semp_simple_multi.scattering.c_infHR[0,:]),np.array([0]))
        [self.assertAlmostEqual(a[0],b[0]) if a-b!=np.array([0.]) else self.assertEqual(a[0],b[0]) for a,b in zip(Semp_simple_mono.scattering.c_0.T,np.atleast_2d(Semp_simple_multi.scattering.c_0[0,:]).T)]
                
    @patch('hydrac.util.display.Display.regularplot')
    def test_scattering_disp(self,dsp_reg):
        self.S1.scatt_func_display()
        dsp_reg.assert_called_once()
        
    def tearDown(self):
        super(TestScattering, self).tearDown()
        patch.stopall()
#    def func_called():



    
    @patch('hydrac.model.water.Water')
    @patch('hydrac.model.particle.Particle')
    def test_class_called(self,MockClass1, MockClass2):
        hydrac.model.particle.Particle()
        hydrac.model.water.Water()
        assert MockClass1 is hydrac.model.particle.Particle
        assert MockClass2 is hydrac.model.water.Water
        assert MockClass1.called
        assert MockClass2.called
    

