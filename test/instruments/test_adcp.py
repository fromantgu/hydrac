#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 17:35:05 2018

@author: fromant9gu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 17:29:39 2018

@author: fromant9gu
"""


import unittest

import sys
import os
#import tkinter as Tk
#from tkinter import filedialog
import unittest.mock as mock
from unittest.mock import patch,Mock,MagicMock
import hydrac
import numpy as np
from hydrac.util.parameters import Parameters
from hydrac.instruments.adcp import Adcp

class TestAdcpA(unittest.TestCase):
    
    @patch('hydrac.instruments.adcp.Adcp.file_select',side_effect=[((os.curdir + '/test/instruments/data_sample_test/test_file_1.PD0',),
 os.curdir + '/test/instruments/data_sample_test',['1']),((os.curdir + '/test/instruments/data_sample_test/test_file_1.PD0',
                                                 os.curdir + '/test/instruments/data_sample_test/test_file_2.PD0',),
 os.curdir + '/test/instruments/data_sample_test',['1','0'])])
    def setUp(self,fake_list):

        if hasattr(TestAdcpA,'A') is False:
            TestAdcpA.A=self.set_Cls()
            TestAdcpA.B=self.set_Cls()

        self.A=TestAdcpA.A
        self.B=TestAdcpA.B 
        self.fake_list=fake_list

    def set_Cls(self):
        return hydrac.instruments.adcp.Adcp('adcp')
    
    def test_base_settings(self):
        self.assertEqual(self.A.tag_M,'mono')
        self.assertEqual(self.fake_list.call_count,2)
        self.assertEqual(len(self.B.filepath),len(self.B.param))
        self.assertEqual(len(self.A.filepath),len(self.A.param))
        self.assertEqual(self.B.param.P0.keys(),self.B.param.P1.keys())
        [[self.assertIn(a,self.A.param['P' + str(i)].adcp) 
          for a in ['north_vel','east_vel','heading','pitch','roll']
          ] for i in range(0,len(self.A.param))]
        [[self.assertIn(a,self.B.param['P' + str(i)].adcp) 
          for a in ['north_vel','east_vel','heading','pitch','roll']
          ] for i in range(0,len(self.B.param))]

    
    def test_calib_settings(self):

        calib=self.A._set_calib_adcp()
        calibB=self.B._set_calib_adcp()
        self.assertEqual(calib,calibB)
        self.assertEqual(self.A.param.P0.adcp.adcp_presets.items(),calib.RDI['f' + str(int(self.A.param.P0.Frequency[0]/1000))].items())
        self.assertEqual(self.B.param.P0.adcp.adcp_presets.items(),calib.RDI['f' + str(int(self.B.param.P0.Frequency[0]/1000))].items())


    def tearDown(self):
        super(TestAdcpA, self).tearDown()
        patch.stopall()
#    def func_called():
        
class TestAdcpP(unittest.TestCase):
    @patch('hydrac.instruments.adcp.Adcp.file_select',side_effect=[((os.curdir + '/test/instruments/data_sample_test/realtimewh-p.000',),
 os.curdir + '/test/instruments/data_sample_test',['p']),((os.curdir + '/test/instruments/data_sample_test/realtimewh-p.000',
                                                 os.curdir + '/test/instruments/data_sample_test/realtimewh-p.000',),
 os.curdir + '/test/instruments/data_sample_test',['p','p'])])
    def setUp(self,fake_list):

        if hasattr(TestAdcpP,'A') is False:
            TestAdcpP.A=self.set_Cls()
            TestAdcpP.B=self.set_Cls()

        self.A=TestAdcpP.A
        self.B=TestAdcpP.B 
        self.fake_list=fake_list

    def set_Cls(self):
        return hydrac.instruments.adcp.Adcp('adcp')
    
    def test_base_settings(self):
        self.assertEqual(self.A.tag_M,'mono')
        self.assertEqual(self.fake_list.call_count,2)
        self.assertEqual(len(self.B.filepath),len(self.B.param))
        self.assertEqual(len(self.A.filepath),len(self.A.param))
        self.assertEqual(self.B.param.P0.keys(),self.B.param.P1.keys())
        [[self.assertIn(a,self.A.param['P' + str(i)].adcp) 
          for a in ['north_vel','east_vel','heading','pitch','roll','latitude','longitude']
          ] for i in range(0,len(self.A.param))]
        [[self.assertIn(a,self.B.param['P' + str(i)].adcp) 
          for a in ['north_vel','east_vel','heading','pitch','roll','latitude','longitude']
          ] for i in range(0,len(self.B.param))]
    
    def test_calib_settings(self):

        calib=self.A._set_calib_adcp()
        calibB=self.B._set_calib_adcp()
        self.assertEqual(calib,calibB)
        self.assertEqual(self.A.param.P0.adcp.adcp_presets.items(),calib.RDI['f' + str(int(self.A.param.P0.Frequency[0]/1000))].items())
        self.assertEqual(self.B.param.P0.adcp.adcp_presets.items(),calib.RDI['f' + str(int(self.B.param.P0.Frequency[0]/1000))].items())


    def tearDown(self):
        super(TestAdcpP, self).tearDown()
        patch.stopall()



    
   
