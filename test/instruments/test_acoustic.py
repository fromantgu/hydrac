#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 17:35:05 2018

@author: fromant9gu
"""

import unittest
import sys

print(sys.path)

# import os

# import tkinter as Tk
# from tkinter import filedialog
# import unittest.mock as mock
from unittest.mock import patch, Mock, MagicMock

import numpy as np

import hydrac
from hydrac.util.parameters import Parameters
# from hydrac.instruments.acoustique import Acoustic
# from hydrac.instruments.mod_deployment import ModeDeployment


def fast_acoustic_object(x, y, z):
    def p_test(x, y, z, t):
        p = Parameters(
            {
                "TransducerKt": kt,
                "NumChannels": 4,
                "RawIntensity": np.ones((x, y, z)),
                "BinRange": np.ones((x, z)),
                "FrequencyRx": f,
                "time": np.arange(y * t + 1, y * t + y + 1),
            }
        )
        return p

    f = np.arange(1, z + 1) * 1e6
    kt = []
    [kt.append([a, b]) for a, b in zip(f, np.multiply(f, 1 / f))]
    A = hydrac.instruments.acoustique.Acoustic()
    A.param.update({"P0": p_test(x, y, z, 0), "P1": p_test(x, y, z, 1)})
    return A


def fast_phy_object(x):
    def p_test(x, t):
        p = Parameters(
            {"at": np.atleast_2d(np.arange(1 + x * t, x * t + x + 1)).T}
        )
        return p

    #    p_test=Parameters({'at': np.atleast_2d(np.arange(1,x*t+x+1)).T})
    A = hydrac.instruments.physicalparam.PhysicalParam()
    A.mode_depl = "Test"
    A.param.update({"P0": p_test(x, 0), "P1": p_test(x, 1)})
    return A


class TestAcoustic(unittest.TestCase):
    counter = 0
    #    def create_patch(self, name):
    #        patcher = patch(name)
    #        thing = patcher.start()
    #        self.addCleanup(patcher.stop)
    #        return thing

    @patch("builtins.input", side_effect=["Test", "10", "Test", "y"])
    @patch("hydrac.instruments.mod_deployment.ModeDeployment")
    @patch("hydrac.model.water.Water")
    @patch("hydrac.instruments.acoustique.Acoustic.spreading_and_att_correction")
    def setUp(self, s_att_sph, mock_w, mock_md, q):
        self.A = fast_acoustic_object(10, 15, 4)
        self.B = fast_acoustic_object(10, 15, 4)
        self.C = fast_acoustic_object(10, 20, 4)

        #        mock_w.water_absorption_coefficient2.return_value=np.ones((1,4))
        mock_w.water_absorption_coefficient.side_effect = [np.ones((4))] * 45
        mm = MagicMock(side_effect=[mock_w, None, mock_w, mock_w])
        s_att_sph.return_value = self.A.param.P0.RawIntensity

        self.s_att_sph = s_att_sph
        self.mock_w = mock_w
        # Case acoustic object alone + water object
        self.A.preproc_acoustic_data(w_obj=mm())

        # Case acoustic object alone + no water object
        self.B.preproc_acoustic_data(w_obj=mm())

        phy_instr = fast_phy_object(20)
        for i in range(0, len(phy_instr.param)):
            phy_instr.param["P" + str(i)].w_obj = mm()
        self.mock_w.water_absorption_coefficient.assert_called_with(
            self.A.param.P0.FrequencyRx
        )
        # Case acoustic object + physical instrument
        self.C.preproc_acoustic_data(phy_instr=phy_instr)


    @patch("builtins.input", side_effect=["10", "10", "Test", "y"])
    @patch("hydrac.model.water.Water")
    def test_rapide(self, ws, q):
        catch_T = float(q())
        self.D = fast_acoustic_object(10, 15, 4)
        ws.water_absorption_coefficient2.return_value = np.ones((1, 4))
        ws.paramw.T = None
        self.D.preproc_acoustic_data(w_obj=ws)
        ws.water_absorption_coefficient2.assert_called_with(
            self.D.param.P0.FrequencyRx, catch_T
        )

    def test_spreading_and_att_correction(self):
        self.assertEqual(
            self.s_att_sph.call_count,
            len(self.A.param) + len(self.B.param) + len(self.C.param),
        )
        self.assertEqual(
            np.unique(self.A.param.P0.Intensity - self.s_att_sph.return_value),
            np.array([0]),
        )

        self.A.param.P0.RawIntensity = self.A.param.P0.RawIntensity + 200
        int_test_1 = self.A.spreading_and_att_correction(i1=0)
        self.assertGreater(
            np.unique(int_test_1)[0], np.unique(self.A.param.P0.Intensity)[0]
        )
        self.assertRaises(
            AttributeError,
            lambda: self.A.spreading_and_att_correction(i1=1, aux=1),
        )

        self.B.param.P0.RawIntensity = self.B.param.P0.RawIntensity + 200
        int_test_2 = self.B.spreading_and_att_correction(i1=0)
        self.assertGreater(
            np.unique(int_test_2)[0], np.unique(self.B.param.P0.Intensity)[0]
        )
        self.assertRaises(
            AttributeError,
            lambda: self.B.spreading_and_att_correction(i1=1, aux=1),
        )

        self.assertNotIn("alphaw", self.C.param.P0.keys())
        self.assertRaises(
            AttributeError,
            lambda: self.C.spreading_and_att_correction(i1=1, aux=None),
        )

        self.assertEqual(
            self.s_att_sph.call_count,
            len(self.A.param) + len(self.B.param) + len(self.C.param),
        )

    def test_preproc(self):
        [
            self.assertIn("Intensity", self.A.param["P" + str(i)].keys())
            for i in range(0, len(self.A.param))
        ]
        #        print('rurururu :{}'.format(type(self.A.param.P0.FrequencyRx)))

        #        self.assertIn(self.A.param.P0.FrequencyRx,self.mock_w.water_absorption_coefficient.call_args_list)
        self.mock_w.water_absorption_coefficient2.assert_not_called()

    def tearDown(self):

        super(TestAcoustic, self).tearDown()
        patch.stopall()


#    def func_called():
