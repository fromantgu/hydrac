#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 17:35:05 2018

@author: fromant9gu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 17:29:39 2018

@author: fromant9gu
"""


import unittest

import sys
import os
#import tkinter as Tk
#from tkinter import filedialog
import unittest.mock as mock
from unittest.mock import patch,Mock,MagicMock
import hydrac
import numpy as np
from hydrac.util.parameters import Parameters
from hydrac.instruments.aquascat import Aquascat
    

class TestAqa(unittest.TestCase):
    counter=0
       
    @patch('builtins.input', side_effect=['n','n','y'])
    @patch('hydrac.instruments.aquascat.Aquascat.file_select',side_effect=[((os.curdir + '/test/instruments/data_sample_test/20160505091541.aqa',),
 os.curdir + '/test/instruments/data_sample_test'),((os.curdir + '/test/instruments/data_sample_test/20160505104359.aqa',
                                                 os.curdir + '/test/instruments/data_sample_test/20160505091541.aqa',),
 os.curdir + '/test/instruments/data_sample_test')])
    def setUp(self,fake_list,q):

        if hasattr(TestAqa,'A') is False:
            TestAqa.A=self.set_Cls()
            TestAqa.B=self.set_Cls()

        self.A=TestAqa.A
        self.B=TestAqa.B 
#        self.A=hydrac.instruments.aquascat.Aquascat('aqa', '')
#        self.B=hydrac.instruments.aquascat.Aquascat('aqa', '')
        self.fake_list=fake_list

    def set_Cls(self):
        return hydrac.instruments.aquascat.Aquascat('aqa')
    
    def test_base_settings(self):
        self.assertEqual(self.A.tag_M,'multi')
        self.assertEqual(self.fake_list.call_count,2)
        self.assertEqual(len(self.A.filepath),len(self.A.param))
        self.assertEqual(self.A.param.P0.TransducerKt[0][1],1)
        self.assertEqual(self.B.param.P0.keys(),self.B.param.P1.keys())
    
    def test_calib_settings(self):

        calib=self.A._set_calib_aqa()
        calibB=self.B._set_calib_aqa()
        self.assertEqual(calib,calibB)
        self.assertIn('SN' +
                       str(self.A.param.P0.SerialNum[0]) +
                       str(self.A.param.P0.SerialNum[1]),calib.keys())
        self.assertIn('SN' +
                       str(self.B.param.P0.SerialNum[0]) +
                       str(self.B.param.P0.SerialNum[1]),calib.keys())
        self.assertEqual(calib['SN' +
                               str(self.A.param.P0.SerialNum[0]) +
                               str(self.A.param.P0.SerialNum[1])].cal_depth1.a,1)
        self.assertEqual(calib['SN' +
                               str(self.B.param.P0.SerialNum[0]) +
                               str(self.B.param.P0.SerialNum[1])].cal_depth1.a,1)
        [self.A._update_calib_aqa('SN' +
                                  str(self.A.param['P' + str(i)].SerialNum[0]) +
                                  str(self.A.param['P' + str(i)].SerialNum[1]),
                                  remove_elem=True) 
                                  for i in range(0, len(self.A.param))]
        [self.B._update_calib_aqa('SN' +
                                  str(self.B.param['P' + str(i)].SerialNum[0]) +
                                  str(self.B.param['P' + str(i)].SerialNum[1]),
                                  remove_elem=True) 
                                  for i in range(0, len(self.B.param))]
        calib=self.A._set_calib_aqa()
        calibB=self.B._set_calib_aqa()
        self.assertNotIn('SN' +
                       str(self.A.param.P0.SerialNum[0]) +
                       str(self.A.param.P0.SerialNum[1]),calib.keys())
        self.assertNotIn('SN' +
                       str(self.B.param.P0.SerialNum[0]) +
                       str(self.B.param.P0.SerialNum[1]),calibB.keys())

    def tearDown(self):
        
        
        super(TestAqa, self).tearDown()
#        patch.stopall()
#    def func_called():



    
   
