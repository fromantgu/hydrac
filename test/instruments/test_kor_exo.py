#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 17:35:05 2018

@author: fromant9gu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 17:29:39 2018

@author: fromant9gu
"""


import unittest

import sys
import os
#import tkinter as Tk
#from tkinter import filedialog
import unittest.mock as mock
from unittest.mock import patch,Mock,MagicMock
import hydrac
import numpy as np
from hydrac.util.parameters import Parameters
from hydrac.instruments.aquascat import Aquascat
    

class TestExo(unittest.TestCase):
    counter=0
       
    @patch('hydrac.instruments.physicalparam.PhysicalParam.preproc_phy_shape')
    @patch('hydrac.instruments.korexo.KorExo.file_select',side_effect=[((os.curdir + '/test/instruments/data_sample_test/exo1.txt',),
 os.curdir + '/test/instruments/data_sample_test'),((os.curdir + '/test/instruments/data_sample_test/exo1.txt',
                                                 os.curdir + '/test/instruments/data_sample_test/exo2.txt',),
 os.curdir + '/test/instruments/data_sample_test')])
    def setUp(self,fake_list,md):

        if hasattr(TestExo,'A') is False:
            TestExo.A=self.set_Cls()
            TestExo.B=self.set_Cls()

        self.A=TestExo.A
        self.B=TestExo.B 
#        self.A=hydrac.instruments.aquascat.Aquascat('aqa', '')
#        self.B=hydrac.instruments.aquascat.Aquascat('aqa', '')
        self.fake_list=fake_list

    def set_Cls(self):
        return hydrac.instruments.korexo.KorExo('exo')
    
    def test_base_settings(self):
        self.assertEqual(self.A.instr_name,'korexo')
        self.assertEqual(self.fake_list.call_count,2)
        self.assertEqual(len(self.A.filepath),len(self.A.param))
        self.assertEqual(self.B.param.P0.keys(),self.B.param.P1.keys())
        self.assertEqual(self.A.param.P0.keys(),self.B.param.P0.keys())
        self.assertEqual(self.B.meta.P0.keys(),self.B.meta.P1.keys())
        p_test=Parameters({'l1':[],'l2':[]})
        [[p_test[j].append(np.size(b)) for a,b in self.B.param['P' + str(i)].items()] for i,j in enumerate(p_test.keys())]
        [self.assertEqual(len(np.unique(p_test[j])),1) for i,j in enumerate(p_test.keys())]
        self.assertNotEqual(np.unique(p_test.l1),np.unique(p_test.l2))

    def tearDown(self):
        
        super(TestExo, self).tearDown()
#        patch.stopall()
#    def func_called():



    
   
