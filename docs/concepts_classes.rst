Concepts, classes and organization of the package
=================================================

Data objects
------------

Hydrac uses data objects. These objects represent particular types of
data. They should be able to be loaded from a file, saved into a file,
displayed to the screen, etc.

These objects are defined in the package :mod:`hydrac.util.parameters`.

"Unit" objects
~~~~~~~~~~~~~~

We can define simple "unit" object as for example:

- Acoustic data
- Metadata extracted from acoustic device
- Physical Parameters data
- Metadata extracted from physical parameters instruments
- Inverted results
- Scattering models
- ...

