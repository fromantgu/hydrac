.. hydrac documentation master file, created by
   sphinx-quickstart on Fri Nov 10 07:50:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hydrac documentation
====================

Hydrac is a libre Python framework for scientific treatments of large
series of active acoustic profil data for the purpose of suspended particulate matter quantification. This package is still a little baby but is expected to grow soon enough to be used by a large panel of users from different horizons. Currently, the package can :
- import and read acoustic binary datas from multifrequency ABS Aquascat, ADCP RDI,
- store the desired data in convenient structures to be further handled 

We want to make hydrac easy (useful documentation, easy installation,
usable with scripts and GUI in Qt), reliable (with good unittests) and very efficient, in
particular when the load of data to treat becomes large. 

User Guide
----------

.. toctree::
   :maxdepth: 1

   overview


Modules Reference
-----------------

Here is presented the general organization of the package (see also
:doc:`concepts_classes`) and the documentation of the modules, classes and
functions.

.. autosummary::
   :toctree: generated/

   hydrac.instruments
   hydrac.inversion
   hydrac.calcul 
   hydrac.io
   hydrac.util
   hydrac.model



More
----

.. toctree::
   :maxdepth: 2

   hydrac forge on Bitbucket <https://bitbucket.org/fromantgu/hydrac>
   bibliography
   to_do
   authors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
