Bibliography
============


**Ainslie, M. A., & McColm, J. G. (1998)**. A simplified formula for viscous and chemical absorption in sea water. The Journal of the Acoustical Society of America, 103(3), 1671–1672.

**Francois, R. E., & Garrison, G. R. (1982)**. Sound absorption based on ocean measurements: Part I: Pure water and magnesium sulfate contributions. The Journal of the Acoustical Society of America, 72(3).

**Gaunaurd, G. C., & Überall, H. (1983)**. RST analysis of monostatic and bistatic acoustic echoes from an elastic sphere. The Journal of the Acoustical Society of America, 73(1), 1–12.
  
**Hurther, D., Thorne, P. D., Bricault, M., Lemmin, U., & Barnoud, J.-M. (2011)**. A multi-frequency Acoustic Concentration and Velocity Profiler (ACVP) for boundary layer measurements of fine-scale flow and sediment transport processes. Coastal Engineering, 58(7), 594–605.

**Maclennan, D., Fernandes, P. G., & Dalen, J. (2002)**. A consistent approach to definitions and symbols in fisheries acoustics. ICES Journal of Marine Science, 59(2), 365–369. https://doi.org/10.1006/jmsc.2001.1158

**Medwin, H., & Clay, C. S. (1997)**. Fundamentals of Acoustical Oceanography. (A. Press, Ed.).

**Morse, P. M., & Ingard, A. U. (1986)**. Theoritical Acoustics (Priceton U).

**Stanton, T. K. (1988)**. Sound scattering by cylinders of finite length. I. Fluid cylinders. The Journal of the Acoustical Society of America, 83(1), 55–63.

**Stanton, T. K. (1989)**. Simple approximate formulas for backscattering of sound by spherical and elongated objects. Journal of Acoustical Society of America, 86(October), 1499–1510.

**Stanton, T. K., Chu, D., & Wiebe, P. H. (1998)**. Sound scattering by several zooplankton groups. II. Scattering models. The Journal of the Acoustical Society of America, 103(1), 236–253.

**Thorne, P. D., & Hanes, D. M. (2002)**. A review of acoustic measurement of small-scale sediment processes. Continental Shelf Research, 22(4), 603–632. https://doi.org/10.1016/S0278-4343(01)00101-7

**Thorne, P. D., MacDonald, I. T., & Vincent, C. E. (2014)**. Modelling acoustic scattering by suspended flocculating sediments. Continental Shelf Research, 88, 81–91. https://doi.org/10.1016/j.csr.2014.07.003



