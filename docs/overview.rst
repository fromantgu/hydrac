Overview
========

Hydrac is a free Python software to compute suspended matter concentration and (if possible) size distribution. Many functionalities can be used as stand alone functions as well to compute scattering models, and many other useful physical quantities

.. toctree::
   :maxdepth: 1

   concepts_classes

